import os 
import re
import pprint

'''
  Collect all headers from 
    - Soil, Weather, Experiment
'''

from parsingConfigs import (
  OUTPUT_PATH,
  HEADER_MEANINGS_FILENAME,
  DSSAT_READ_PATH,
  DSSAT_CODES_READ_PATH,
  SOIL_READ_PATH,
  WEATHER_READ_PATH, 
  EXPERIMENT_READ_PATH,
  DSSAT_GENOTYPES_READ_PATH
)

from helperFxns import (
  readFile,
  writeToFile,
  checkSubstring,
  countEndString
)

meanings = {}
headerDict = {}
dssatOutputHeaders = {}

STRIP_THESE = '[\u00b0]'
regexObj = re.compile(STRIP_THESE)

def createDictHeaderMeanings(rawContents, positions=[9, 78]): 
  outputDict = {}

  for string in rawContents: 
    if(string
       and not string.startswith('*')
       and not string.startswith('!')
       and not string.startswith('@')): 
      
      # The range of description string is from [23:80]
      desc = string[positions[0]:positions[1]].strip()
      desc = regexObj.sub('', desc)
      tokens = string.split()

      outputDict[tokens[0]] = desc
    
  return outputDict

def getHeaders(filetype): 
  extensions = {
    'CLIMATE': '.WTG',
    'WEATHER': '.WTH',
    'SOIL': '.SOL',
    'EXPERIMENT': 'X',
    'GENOTYPE': '.CUL'
  }

  EXCLUDE_THIS = [
    'PEOPLE', 'ADDRESS', 'METHODS', 'INSTRUMENTS', 
    'PROBLEMS', 'PUBLICATIONS', 'DISTRIBUTION', 'NOTES',
    'SITE', 'AUTOMATIC'
  ]

  outputDict = {}
  readPath = ''
  if filetype == 'WEATHER': readPath = WEATHER_READ_PATH
  elif filetype == 'SOIL': readPath = SOIL_READ_PATH
  elif filetype == 'EXPERIMENT': readPath = EXPERIMENT_READ_PATH
  
  for filename in os.listdir(readPath): 
    if filename.endswith(extensions[filetype]): 
      contents = readFile(readPath + filename)

      for string in contents: 
        if(string 
           and string.startswith('@')
           and not checkSubstring(string, EXCLUDE_THIS, True)):
          # Remove the @ sign 
          cleanedString = regexObj.sub('', string).strip()
          cleanedString = ','.join(cleanedString.split())
          # tokens = cleanedString.split()
          # for token in tokens: 
          if not cleanedString in outputDict: 
            outputDict[cleanedString] = filename

  return outputDict

def parseMeaningDictionary(dssatoutput=False): 
  outputDict = {}
  for filename in os.listdir(DSSAT_CODES_READ_PATH): 
    if filename.endswith('.CDE'): 
      code = filename.split('.')[0]
      contents = readFile(DSSAT_CODES_READ_PATH + filename)
      if dssatoutput: 
        outputDict[code] = createDictHeaderMeanings(contents, [21, 80])
      else: 
        outputDict[code] = createDictHeaderMeanings(contents)

  return outputDict

def createHeaderMeaningDict(headerList, meaningDict): 
  outputDict = {}
  for k, v in headerList.items(): 
    tokens = k.split(',')
    for token in tokens: 
      if not token in outputDict: 
        if token in meaningDict: 
          outputDict[token] = meaningDict[token]
        else: 
          outputDict[token] = v
  
  return outputDict

def parseAllHeaders(writeFlag=False): 
  headers = {}

  meaningDict = parseMeaningDictionary()
  
  headers['WEATHER'] = createHeaderMeaningDict(getHeaders('WEATHER'), meaningDict['WEATHER'])
  headers['SOIL'] = createHeaderMeaningDict(getHeaders('SOIL'), meaningDict['SOIL'])
  headers['EXPERIMENT'] = createHeaderMeaningDict(getHeaders('EXPERIMENT'), meaningDict['DETAIL'])

  if writeFlag: 
    writeToFile(OUTPUT_PATH + HEADER_MEANINGS_FILENAME, headers, 'w', False, True, True)

  print('Header Lengths ----')
  print('Weather: ' + str(len(headers['WEATHER'])))
  print('Weather Parameters with meanings: ' + str(len(headers['WEATHER']) - countEndString(headers['WEATHER'], '.WTH')))
  print('Soil: ' + str(len(headers['SOIL'])))
  print('Soil Parameters with meanings: ' + str(len(headers['SOIL']) - countEndString(headers['SOIL'], '.SOL')))
  print('Experiment: ' + str(len(headers['EXPERIMENT'])))
  print('Experiment Parameters with meanings: ' + str(len(headers['EXPERIMENT']) - countEndString(headers['EXPERIMENT'], '.SBX')))

parseAllHeaders(True)