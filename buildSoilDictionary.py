import os
import re
import pprint
import pandas as pd
import numpy as np
from io import StringIO

'''
  TODO: 
    - [x] Remove all soil tables with problems from the soil dict.
    - [ ] For some reason, DSSAT is not reading the WW.SOL. Exclude this from parsing. 

  NOTE
    - Shape of df when soil genInfo data is added together: (3442, 10)
    - Count of null values per parameter
      - SCOM    101
      - SALB      0
      - SLU1      2
      - SLDR      0
      - SLRO      0
      - SLNF      0
      - SLPF      0
      - SMHB     11
      - SMPX     11
      - SMKE     11
    - I decided not to remove any parameters. 
    - Shape of df when soil level data is added together: (16913, 17)
    - Count of null values per parameter
      - SLB         0
      - SLMH     1836
      - SLLL        1
      - SDUL        1
      - SSAT        1
      - SRGF        0
      - SSKS      817
      - SBDM        8
      - SLOC     1430
      - SLCL      559
      - SLSI      569
      - SLCF    14198
      - SLNI     4653
      - SLHW      882
      - SLHB    10976
      - SCEC     2139
      - SADC    16905
    - Due to this, I decided to remove these parameters: 
      - SLCF
      - SLHB
      - SADC  
'''

pd.set_option('display.expand_frame_repr', False)

from helperFxns import (
  readFile, 
  readStringCSV,
  joinTokens,
  checkSubstring, 
  pickleDictionary,
  checkStringStart,
)

from countingFxns import (
  checkHeaders, 
  checkTokenCount, 
  getPandasDFSizes
)

from parsingConfigs import (
  SOIL_CHECKSUM_FLAG, 
  OUTPUT_PATH, 
  SOIL_READ_PATH, 
  WEATHER_READ_PATH, 
  CLEANED_SOIL_FILENAME, 
  EXCLUDE_SOIL_TABLE,
  CLEANED_FILE_PATH,
  INCOMPLETE_SOIL,
  GENINFO, 
  LVLONE,  
  LVLTWO,  
  SOILLEVELS,
)

pp = pprint.PrettyPrinter(width=200) 

def getAllCompleteSoilData(soilTables): 
  cols = ['SLB', 'SLLL', 'SDUL', 'SSAT', 'SRGF', 
          'SBDM', 'SLOC', 'SLHW', 'SLCL', 'SLSI', 'SLNI' ]

  incompleteSoils = []
  completeSoils = []

  for key, val in soilTables.items(): 
    data = val[LVLONE]
    ddf = pd.read_csv(StringIO(data), sep=';')
    df = ddf[cols]

    null_columns = df.columns[df.isnull().any()]
    if len(null_columns) != 0: 
      incompleteSoils.append(key)
    else: 
      completeSoils.append(key)

  print('Incomplete: ')
  print(incompleteSoils)
  # print('Complete: ' + str(len(completeSoils)))

# Accepts a list of strings from a file. 
def segregateSoilSections(rawContents): 
  tables = []
  dummyList = []
  parsedCodes = []
  sectionStartFlag = False

  # The code is alphanumeric (with _) and starts with *
  regexString = '^\*[A-Z0-9a-z_]+.*'    
  # Exclude the entries that starts with soils. 
  regexString2 = '^(!|\*)+(==|SOILS)+.*'

  # Compile the regex for performance. 
  regexObj = re.compile(regexString)
  regexObj2 = re.compile(regexString2, flags=re.I)

  EXCLUDE_THIS = ['!', '==', '*SOIL']

  for string in rawContents: 
    # Check if the string is empty or a comment string. 
    if(string and not checkStringStart(string, EXCLUDE_THIS)): 
      if not sectionStartFlag: 
        if(regexObj.match(string) 
           and not regexObj2.match(string)
           and not EXCLUDE_SOIL_TABLE in string): 
          sectionStartFlag = True
          parsedCodes.append(string.split()[0])
      
      if sectionStartFlag: 
        dummyList.append(string)
    else: 
      # Protection against comments at the 
      # middle of a table (e.g CO.SOL)
      if(sectionStartFlag 
         and len(dummyList) > 6): 
        sectionStartFlag = False
        tables.append(dummyList)
        dummyList = []

  # Append the last table. 
  if len(dummyList) > 0:
    tables.append(dummyList)

  return [tables, parsedCodes]

def processSections(segregatedSections, name=None): 
  WORD_ARRAY1 = ['SCOM', 'SALB', 'SLDR']
  WORD_ARRAY2 = ['SLB', 'SLMH', 'SLLL', 'SDUL']
  WORD_ARRAY3 = ['SLB', 'SLPX', 'SLPT']
  NULL_SIGNS  = ['-99', '?', '-', ]

  fileDictionary = {}

  for sections in segregatedSections: 
    currentHeader = ''
    sectionSoilID = ''
    sectionDictionary = {}
    stringAccumulator = []
    SLBIndexStart = 0

    for string in sections: 
      if not currentHeader and string.startswith('*'):
        sectionSoilID = string.split()[0]
        # Remove the asterisk at the start. 
        sectionSoilID = sectionSoilID[1:]
        continue

      # If header. 
      if string.startswith('@'): 
        header = joinTokens(string[1:].upper().split())
        
        if checkSubstring(string, WORD_ARRAY1):
          currentHeader = GENINFO
        elif checkSubstring(string, WORD_ARRAY2): 
          currentHeader = LVLONE
          # For separating 'SLB' and 'SLMH'
          SLBIndexStart = string.index('SLB')
        elif checkSubstring(string, WORD_ARRAY3): 
          sectionDictionary[LVLONE] = joinTokens(stringAccumulator, '\n')
          stringAccumulator.clear()
          currentHeader = LVLTWO

        if currentHeader: 
          stringAccumulator.append(header)
      else: 
        if currentHeader: 
          tokens = string.strip().split()
          # Resolve unparsable values in strings. 
          if currentHeader == LVLONE: 
            SLMHCodeStart = SLBIndexStart + 3
            tokens[0] = string[0:SLMHCodeStart].strip()
            tokens[1] = string[SLMHCodeStart:SLMHCodeStart + 6].strip()
          
          # Remove null values
          tokens = [x if not checkSubstring(x, NULL_SIGNS, True) else '' for x in tokens]
          csvString = joinTokens(tokens)
          stringAccumulator.append(csvString)

          if currentHeader == GENINFO: 
            sectionDictionary[GENINFO] = joinTokens(stringAccumulator, '\n')
            stringAccumulator.clear()
    
    sectionDictionary[currentHeader] = joinTokens(stringAccumulator, '\n')
    fileDictionary[sectionSoilID] = sectionDictionary

  return fileDictionary
      
def mergeTables(soilTables): 
  outputDict = {}
  for code, tables in soilTables.items(): 
    outputDict[code] = {}
    for key in tables: 
      outputDict[code][key] = {}
      val = tables[key]

      try:
        soilLevelDF = readStringCSV(val[LVLONE], index='SLB')
        if LVLTWO in val: 
          table2 = readStringCSV(val[LVLTWO], index='SLB')
          # Merge table 1 and table 2 with index as 'SLB'
          soilLevelDF = pd.concat([soilLevelDF, table2], axis=1)
          soilLevelDF.reset_index()

        outputDict[code][key][GENINFO] = val[GENINFO]
        outputDict[code][key][SOILLEVELS] = soilLevelDF.to_csv(sep=';')
      except pd.errors.ParserError:
        print('%s: Error Parsing' % key)

  return outputDict

def removeProblematicSoilTables(bufferStringDict):
  bufferStringDictCopy = bufferStringDict.copy()
  probKeys = checkTokenCount(bufferStringDict)
  for key in probKeys:
    del(bufferStringDictCopy[key])

  bufferStringDict = bufferStringDictCopy.copy()
  headerStrings = checkHeaders(bufferStringDict)

  for key, val in bufferStringDict.items(): 
    headerLVLONE = val[LVLONE].split('\n')[0]
    headerGENINFO = val[GENINFO].split('\n')[0]

    if(not headerLVLONE == headerStrings['LVLONE'] or 
       not headerGENINFO == headerStrings['GENINFO']):
      del(bufferStringDictCopy[key])

  for key in INCOMPLETE_SOIL: 
    if key in bufferStringDictCopy: 
      del bufferStringDictCopy[key]

  return bufferStringDictCopy

def mergeAllData(bufferStringsList): 
  dfAccumulator = pd.DataFrame()

  for val in bufferStringsList: 
    dataframe = pd.read_csv(StringIO(val), sep=';')
    dfAccumulator = dfAccumulator.append(dataframe, ignore_index=True)

  return dfAccumulator

def describeSoilData(bufferStringsDict): 
  bfStringsSoilData = [v[LVLONE] for k, v in bufferStringsDict.items()]
  bfStringsGenInfo = [v[GENINFO] for k, v in bufferStringsDict.items()]

  dfSoilData = mergeAllData(bfStringsSoilData)
  print(dfSoilData.describe())
  # Count null values. 
  # print(dfSoilData.isnull().sum())
  # print(dfSoilData.shape)

def segregateBySize(bufferStringsDict, sizes=[6, 5, 4, 3]): 
  '''
    NOTE: Assumes that fastDataBuild() has been executed. 
    Returns a dict with keys of allowed soil sizes.
    The values are list of soil keys. 
  '''
  outputDict = {}

  for k, v in bufferStringsDict.items():
    length = len(v[LVLONE].split('\n')) - 1
    if length in sizes:  
      if not length in outputDict: 
        outputDict[length] = []

      outputDict[length].append(k)

  return outputDict

def fastDataBuild(writeFlag=False, checkFlag=False, removeFlag=True, filename=None, test=False):
  '''
    Returns a dict  
      - keys - soil keys
      - values - (type - dict)
        - LVLONE - soil profile data. csv-like string that 
            can be read by pandas read_csv() via StringIO
        - GENINFO - soil general info of the soil. same with LVLONE

    - arguments
      - writeflag - write output to file. 
      - checkflag - checks the consistency and correctness of the
          parsed soil data. 
      - removeFlag - removes soil tables that will likely cause troubles. 
      - filename - filename of file <SOILNAME.SOL> you only want to parse.
          Returns the soil tables contained only on that file. 
  '''

  processedSoilTables = {} 
  directory = [x.upper() for x in os.listdir(SOIL_READ_PATH)]

  if filename: 
    content = readFile(SOIL_READ_PATH + filename, True)
    [sectionsArray, codeList] = segregateSoilSections(content)
    processedSoilTables.update(processSections(sectionsArray))
  else: 
    for filename in directory: 
      if(filename.endswith('.SOL')
         and not filename.startswith('WW')):  
        content = readFile(SOIL_READ_PATH + filename, True)
        [sectionsArray, codeList] = segregateSoilSections(content)
        processedSoilTables.update(processSections(sectionsArray))

    # if writeFlag: writeOutput(processedSoilTables)

    if removeFlag: 
      processedSoilTables = removeProblematicSoilTables(processedSoilTables)

    if checkFlag: 
      getPandasDFSizes(processedSoilTables, checkFlag)
      checkHeaders(processedSoilTables, checkFlag)
      checkTokenCount(processedSoilTables, checkFlag)

    # describeSoilData(processedSoilTables)
    if writeFlag: 
      filepath = CLEANED_FILE_PATH + CLEANED_SOIL_FILENAME
      pickleDictionary(filepath, processedSoilTables, True)
      # bfStringsSoilData = [v[LVLONE] for k, v in processedSoilTables.items()]
      # df = mergeAllData(bfStringsSoilData)
      # df.to_csv(OUTPUT_PATH + 'soilDictDF.csv')

    if test: 
      getAllCompleteSoilData(processedSoilTables)
  
  return processedSoilTables

# fastDataBuild(True)
# fastDataBuild(test=True)
