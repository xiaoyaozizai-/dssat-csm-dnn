import os
import sys
import numpy as np
import pandas as pd
import pprint as pp

from io import StringIO

from buildSoilDictionary import fastDataBuild, segregateBySize
from buildExperimentDictionary import buildExperimentDictionary
from buildWeatherDictionary import parseWeatherList

from parsingConfigs import (
  SOIL_LEVEL,
  WEATHER_READ_PATH,
  OUTPUT_PATH,
  PLANTING_DIST,
  PLANTING_METHODS,
  PD_RANGES,
  LVLONE
)

from helperFxns import (
  joinTokens,
  writeToFile
)

from experimentFileConstants import (
  CULTIVAR_LENGTH,
  CULTIVAR_STRINGS,
  CROP_FILE_EXT
)

pd.set_option('display.expand_frame_repr', False)

def gc(type, key): 
  CONSTANT_STRINGS = {
    'POS': {
      'TRTMNTS' : [0, 3, 5, 7, 9, 35, 38, 41, 44, 47, 50, 53, 56, 59, 62, 65, 68, 71],
      'FLD': [0, 3, 12, 22, 28, 34, 40, 46, 52, 57, 63, 69, 81],
      'FLD_2': [0, 3, 19, 35, 45, 63, 69, 75, 81, 87],
      'PD': [0, 3, 9, 16, 22, 28, 34, 40, 46, 52, 58, 64, 70, 76, 82, 110],
      'SIM': [0, 3, 15, 21, 27, 33, 39, 45, 71],
      'SIM_2': [0, 3, 15, 21, 27, 33, 39, 45, 52, 58, 65],
      'SIM_3': [0, 3, 15, 21, 27, 33, 39, 45, 51, 57, 63, 69, 75],
      'SIM_4': [0, 3, 15, 21, 27, 33, 39],
      'SIM_5': [0, 3, 15, 21, 27, 33, 39, 45, 51, 57, 63, 69, 75, 81, 87],
      'AM': [0, 3, 15, 21, 27, 33, 39, 45, 51],
      'AM_2': [0, 3, 15, 21, 27, 33, 39, 45, 51],
      'AM_3': [0, 3, 15, 21, 27, 33, 39],
      'AM_4': [0, 3, 15, 21, 27],
      'AM_5': [0, 3, 15, 21, 27, 33]  
    },
    'STR': {
      'SIM_H'    : '@N GENERAL     NYERS NREPS START SDATE RSEED SNAME.................... SMODEL',
      'SIM_H_2'  : '@N OPTIONS     WATER NITRO SYMBI PHOSP POTAS DISES  CHEM  TILL   CO2', 
      'SIM_H_3'  : '@N METHODS     WTHER INCON LIGHT EVAPO INFIL PHOTO HYDRO NSWIT MESOM MESEV MESOL',
      'SIM_H_4'  : '@N MANAGEMENT  PLANT IRRIG FERTI RESID HARVS',
      'SIM_H_5'  : '@N OUTPUTS     FNAME OVVEW SUMRY FROPT GROUT CAOUT WAOUT NIOUT MIOUT DIOUT VBOSE CHOUT OPOUT',
      'SIM_SI'   : '1 GE              1     1     S 63121  2150 DEFAULT SIMULATION CONTR',
      'SIM_SI_2' : ' 1 OP              Y     Y     Y     N     N     N     N     Y     M',
      'SIM_SI_3' : ' 1 ME              M     M     E     R     S     L     R     1     G     R     2',
      'SIM_SI_4' : ' 1 MA              R     R     R     R     M',
      'SIM_SI_5' : ' 1 OU              N     Y     Y     1     Y     Y     Y     Y     N     N     Y     N     N',
      'AM_H'     : '@N PLANTING    PFRST PLAST PH2OL PH2OU PH2OD PSTMX PSTMN',
      'AM_H_2'   : '@N IRRIGATION  IMDEP ITHRL ITHRU IROFF IMETH IRAMT IREFF',
      'AM_H_3'   : '@N NITROGEN    NMDEP NMTHR NAMNT NCODE NAOFF',
      'AM_H_4'   : '@N RESIDUES    RIPCN RTIME RIDEP',
      'AM_H_5'   : '@N HARVEST     HFRST HLAST HPCNP HPCNR',
      'AM_SI'    : ' 1 PL            -99   -99    40   100    30    40    10',
      'AM_SI_2'  : ' 1 IR             30    50   100 GS000 IR001    10     1',
      'AM_SI_3'  : ' 1 NI             30    50    25 FE001 GS000',
      'AM_SI_4'  : ' 1 RE            100     1    20',
      'AM_SI_5'  : ' 1 HA              0   -99   100     0',
    }, 
    'TOKENS': {
      'FLD_H'    : ['@L', 'ID_FIELD', 'WSTA....', 'FLSA', 'FLOB', 'FLDT', 'FLDD', 'FLDS', 
                    'FLST', 'SLTX', 'SLDP', 'ID_SOIL', 'FLNAME'],
      'FLD_SI'   : ['<FieldLevel>', '<FieldID>', '<WeatherStationCode>', '-99', '0', 
                    'IB000', '0', '0', '0', '-99', '182', '<SoilKey>', '<FieldName>'],
      'FLD_2_H'  : ['@L', '...........XCRD', '...........YCRD', '.....ELEV', 
                    '.............AREA', '.SLEN', '.FLWR', '.SLAS', 'FLHST', 'FHDUR'],
      'FLD_2_SI' : ['1', '-99', '-99', '-99', '-99','-99','-99','-99','-99','-99'],
      'PD_H'     : ['@P', 'PDATE', 'EDATE', 'PPOP', 'PPOE', 'PLME', 'PLDS', 'PLRS', 
                    'PLRD', 'PLDP', 'PLWT', 'PAGE', 'PENV', 'PLPH', 'SPRL', 'PLNAME'], 
      'PD_SI'    : ['1', '85140', '-99', '25.3', '25.3', 'S', 'R', '74', '0' '4', 
                    '-99', '-99', '-99', '-99', '-99', '-99', '-99'],
      'TRT_H'    : ['@N',' R',' O', 'C', 'TNAME....................', 'CU', 'FL', 'SA', 
                    'IC', 'MP', 'MI', 'MF', 'MR', 'MC', 'MT', 'ME', 'MH', 'SM'],
      'TRT_SI'   : ['<TreatmentLevel>', '1', '0', '0', '<Treatment Name>', '<CultivarLevel>', 
                    '<FieldLevel>', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1'],
      'SIM_SI'   : ['1', 'GE', '1', '1', 'S', '<start_date>', '<# of seeds?>', '<simulation name>', '-99'],
      'SIM_SI_2' : ['1', 'OP', 'Y', 'Y', 'Y', 'N', 'N', 'N', 'N', 'Y', 'M'],
      'SIM_SI_3' : ['1', 'ME', 'M', 'M', 'E', 'R', 'S', 'L', 'R', '1', 'G', 'R', '2'],
      'SIM_SI_4' : ['1', 'MA', 'R', 'R', 'R', 'R', 'M'],
      'SIM_SI_5' : ['1', 'OU', 'N', 'Y', 'Y', '1', 'Y', 'Y', 'Y', 'Y', 'N', 'N', 'Y', 'N', 'N'],
      'AM_SI'    : ['1', 'PL', '-99', '-99', '40', '100', '30', '40', '10'],
      'AM_SI_2'  : ['1', 'IR', '30', '50', '100', 'GS000', 'IR001', '10', '1'],
      'AM_SI_3'  : ['1', 'NI', '30', '50', '25', 'FE001', 'GS000'],
      'AM_SI_4'  : ['1', 'RE', '100', '1', '20'],
      'AM_SI_5'  : ['1', 'HA', '0', '-99', '100', '0'],
    }
  }

  return CONSTANT_STRINGS[type][key]

def getDigitNum(x): 
  if   x > 99: return 3
  elif x > 9: return 2
  elif x > 0: return 1
  else: return 0

def createFieldTableTwo(rowSize): 
  count = list(range(1, rowSize + 1))
  df = pd.DataFrame([gc('TOKENS', 'FLD_2_SI')] * rowSize, 
                    columns=gc('TOKENS', 'FLD_2_H'))
  df['@L'] = count

  return df

def appendCultivarTable(fp, cropCode): 
  fp.write(joinTokens(CULTIVAR_STRINGS[cropCode], '\n'))
  fp.write('\n\n')

def createFieldDummyEntries(rowSize, fieldIDPrefix, soilLevel): 
  soilData = fastDataBuild()
  weatherKeys = parseWeatherList()
  soilKeysDict = segregateBySize(soilData)

  # Get all the soil keys with n levels. 
  soilKeys = soilKeysDict[soilLevel].copy()

  count = list(range(1, rowSize + 1))
  weatherRandom = np.random.randint(0, len(weatherKeys), rowSize)
  weatherRandom = [weatherKeys[x] for x in weatherRandom]
  soilRandom = np.random.randint(0, len(soilKeys), rowSize)
  soilRandom = [soilKeys[x] for x in soilRandom]
  plantingDepths = []

  for key in soilRandom: 
    soilRows = soilData[key][LVLONE].split('\n')
    plantingDepths.append(soilRows[-1].split(';')[0])

  fieldID = [fieldIDPrefix + '0'*(4 - getDigitNum(x)) + str(x) for x in range(1, rowSize + 1)]
  fieldName = ['Dummy Field ' + '0'*(4 - getDigitNum(x)) + str(x) for x in range(1, rowSize + 1)]

  df = pd.DataFrame([gc('TOKENS', 'FLD_SI')] * rowSize, 
                     columns=gc('TOKENS', 'FLD_H'))

  df['@L'] = count
  df['ID_FIELD'] = fieldID
  df['WSTA....'] = weatherRandom
  df['ID_SOIL'] = soilRandom
  df['FLNAME'] = fieldName
  df['SLDP'] = plantingDepths

  return df

def createPlantingDetailsDummyEntries(chosenFieldLevels, chosenWeatherStationsDF): 
  PD_HEADER = gc('TOKENS', 'PD_H')
  PD_SAMPLE_INPUT = gc('TOKENS', 'PD_SI')

  date, ppop, plrs, plrd, pldp = PD_RANGES.values()

  stringAccumulator = []
  rowSize = len(chosenFieldLevels)

  # Appnend header.   
  stringAccumulator.append(joinTokens(PD_HEADER))

  # 1 - 200 days only since handling over the year planting seasons is troublesome. 
  randomDates = np.random.randint(date[0], date[1], rowSize)
  pldsRandom = np.random.randint(0, len(PLANTING_DIST), rowSize)
  plmeRandom = np.random.randint(0, len(PLANTING_METHODS), rowSize)
  pldsRandom = [PLANTING_DIST[x] for x in pldsRandom]
  plmeRandom = [PLANTING_METHODS[x] for x in plmeRandom]
  ppopRandom = np.random.randint(ppop[0], ppop[1], rowSize)
  plrsRandom = np.random.randint(plrs[0], plrs[1], rowSize)
  plrdRandom = np.random.randint(plrd[0], plrd[1], rowSize)
  pldpRandom = np.random.randint(pldp[0], pldp[1], rowSize)
  count = list(range(1, rowSize + 1))

  plantingStarts = []

  for i in range(0, rowSize): 
    # Extract weather station from the dataframe.
    fieldLevel = chosenFieldLevels[i]
    weatherStationRow = chosenWeatherStationsDF[chosenWeatherStationsDF['@L'] == fieldLevel]
    weatherStation = weatherStationRow['WSTA....'].values[0]

    # Extract year. 
    plantingYear = weatherStation[-4:-2]

    if(randomDates[i] == 0):
      plantingStarts.append(plantingYear + '000')
    else: 
      plantingDay = '0'*(3 - getDigitNum(randomDates[i])) + str(randomDates[i])
      plantingStarts.append(plantingYear + plantingDay)

  df = pd.DataFrame([PD_SAMPLE_INPUT] * rowSize, columns=PD_HEADER)

  df['@P'] = count
  df['PDATE'] = plantingStarts
  df['PPOP'] = ppopRandom
  df['PPOE'] = ppopRandom
  df['PLRS'] = plrsRandom
  df['PLME'] = plmeRandom
  df['PLDS'] = pldsRandom
  df['PLRD'] = plrdRandom
  df['PLDP'] = pldpRandom

  return df

def writeNewLines(fp, count=1): 
  fp.write('\n'*count)

def writeOneRowTable(fp, header, row): 
  fp.write(header + '\n')
  fp.write(row)

def writeSimulationTables(fp, pdates): 
  sep = ';'
  tableNum = len(pdates)
  seedNumRandom = np.random.randint(300, 2500, tableNum)
  simName = ['Dummy Simulation ' + '0'*(4 - getDigitNum(i)) + str(i) for i in range(1, tableNum + 1)]

  for table in range(0, tableNum): 
    row =   gc('TOKENS', 'SIM_SI')
    row_2 = gc('TOKENS', 'SIM_SI_2')
    row_3 = gc('TOKENS', 'SIM_SI_3')
    row_4 = gc('TOKENS', 'SIM_SI_4')
    row_5 = gc('TOKENS', 'SIM_SI_5')
    row_6 =  gc('TOKENS', 'AM_SI')
    row_7 =  gc('TOKENS', 'AM_SI_2')
    row_8 =  gc('TOKENS', 'AM_SI_3')
    row_9 =  gc('TOKENS', 'AM_SI_4')
    row_10 = gc('TOKENS', 'AM_SI_5')

    row[0] = row_2[0] = row_3[0] = row_4[0] = row_5[0] = str(table + 1)
    row_6[0] = row_7[0] = row_8[0] = row_9[0] = row_10[0] = str(table + 1)

    row[5] = pdates[table]
    row[6] = str(seedNumRandom[table])
    row[7] = simName[table]

    bfString = joinTokens(row)
    bfString2 = joinTokens(row_2)
    bfString3 = joinTokens(row_3)
    bfString4 = joinTokens(row_4)
    bfString5 = joinTokens(row_5)
    bfString6 = joinTokens(row_6)
    bfString7 = joinTokens(row_7)
    bfString8 = joinTokens(row_8)
    bfString9 = joinTokens(row_9)
    bfString10 = joinTokens(row_10)

    input_1  = createIndexSensitiveString(bfString,   gc('POS', 'SIM'),   len(gc('STR', 'SIM_H')),   sep) + '\n'
    input_2  = createIndexSensitiveString(bfString2,  gc('POS', 'SIM_2'), len(gc('STR', 'SIM_H_2')), sep) + '\n'
    input_3  = createIndexSensitiveString(bfString3,  gc('POS', 'SIM_3'), len(gc('STR', 'SIM_H_3')), sep) + '\n'
    input_4  = createIndexSensitiveString(bfString4,  gc('POS', 'SIM_4'), len(gc('STR', 'SIM_H_4')), sep) + '\n'
    input_5  = createIndexSensitiveString(bfString5,  gc('POS', 'SIM_5'), len(gc('STR', 'SIM_H_5')), sep) + '\n'
    input_6  = createIndexSensitiveString(bfString6,  gc('POS', 'AM'),    len(gc('STR', 'AM_H')),    sep) + '\n'
    input_7  = createIndexSensitiveString(bfString7,  gc('POS', 'AM_2'),  len(gc('STR', 'AM_H_2')),  sep) + '\n'
    input_8  = createIndexSensitiveString(bfString8,  gc('POS', 'AM_3'),  len(gc('STR', 'AM_H_3')),  sep) + '\n'
    input_9  = createIndexSensitiveString(bfString9,  gc('POS', 'AM_4'),  len(gc('STR', 'AM_H_4')),  sep) + '\n'
    input_10 = createIndexSensitiveString(bfString10, gc('POS', 'AM_5'),  len(gc('STR', 'AM_H_5')),  sep) + '\n'

    writeOneRowTable(fp, gc('STR', 'SIM_H'), input_1)
    writeOneRowTable(fp, gc('STR', 'SIM_H_2'), input_2)
    writeOneRowTable(fp, gc('STR', 'SIM_H_3'), input_3)
    writeOneRowTable(fp, gc('STR', 'SIM_H_4'), input_4)
    writeOneRowTable(fp, gc('STR', 'SIM_H_5'), input_5)
    writeNewLines(fp, 2)
    fp.write('@  AUTOMATIC MANAGEMENT\n')
    writeOneRowTable(fp, gc('STR', 'AM_H'), input_6)
    writeOneRowTable(fp, gc('STR', 'AM_H_2'), input_7)
    writeOneRowTable(fp, gc('STR', 'AM_H_3'), input_8)
    writeOneRowTable(fp, gc('STR', 'AM_H_4'), input_9)
    writeOneRowTable(fp, gc('STR', 'AM_H_5'), input_10)
    writeNewLines(fp, 2)

def createTreatmentDummyEntries(rowSize, fieldSize, cultivarSize, firstExperimentFlag=False): 
  TREATMENT_TABLE_HEADER = gc('TOKENS', 'TRT_H')
  TREATMENT_TABLE_INPUT = gc('TOKENS', 'TRT_SI')

  if firstExperimentFlag: 
    # To ensure that all cultivars will be used and get the same number of 
    # encoded categorical variables in the training data. 
    cultivarRange = list(range(1, cultivarSize + 1))
    randNum = np.random.randint(1, cultivarSize, rowSize - len(cultivarRange))
    cultivarRandom = np.concatenate([cultivarRange, randNum])
  else: 
    cultivarRandom = np.random.randint(1, cultivarSize, rowSize)

  fieldRandom = np.random.randint(1, fieldSize, rowSize)
  count = list(range(1, rowSize + 1))
  tname = ['Dummy Treatment ' + '0'*(4 - getDigitNum(x)) + str(x) for x in range(1, rowSize + 1)]

  df = pd.DataFrame([TREATMENT_TABLE_INPUT] * rowSize, columns=TREATMENT_TABLE_HEADER)
  df['TNAME....................'] = tname
  df['@N'] = count
  df['CU'] = cultivarRandom
  df['FL'] = fieldRandom
  df['MP'] = count
  df['SM'] = count

  return df

def createExperimentSpecifics(fp, locationString, data): 
  '''
    Writes the strings, comments and tables that won't be randomized. 
  '''
  if locationString == 'START': 
    cropCode = data['CROP_CODE']
    experimentCode = data['EXPERIMENT_CODE']

    fluffStrings = '*GENERAL\n@PEOPLE\nbjmrevilla\n@ADDRESS\nUPLB\n@SITE\nLos Banos, Laguna\n\n'
    treatmentString = '*TREATMENTS                        -------------FACTOR LEVELS------------'

    fp.write('*EXP.DETAILS: %s%s BJMREVILLA SPECIAL PROBLEM UPLB\n\n' % (experimentCode, cropCode))
    fp.write(fluffStrings)
    fp.write(treatmentString + '\n')
  elif locationString == 'END': 
    pass

def getFormattedDF(df):
  rows = df.to_csv(index=False).split('\n')
  # Remove empty strings. 
  rows = [row for row in rows if row]
  return rows

def writeTable(fp, rowList, newlineCount=1): 
  for row in rowList: 
    fp.write(row + '\n')

  fp.write('\n' * newlineCount)

def createIndexSensitiveString(string, positions, maxLength, sep=','): 
  '''
    - Changes the position of the tokens based on the given positions.
    - arguments
      - string - string to be processed. 
      - positions - index positions based on the header. 
  '''

  tokens = string.split(sep)
  tokens = [token.strip() for token in tokens]

  # Create string with the same length as the string. 
  if maxLength > len(string): 
    tempString = ' ' * maxLength
  else: 
    tempString = ' ' * len(string)
    
  # Append the tokens based on position. 
  for i in range(0, len(tokens)): 
    tempString = tempString[:positions[i]] + tokens[i] + tempString[positions[i]:]

  return tempString.rstrip()

def createDSSATFormatTable(csv_rows, positions): 
  dssatFormattedRows = []
  maxLength = len(csv_rows[0])

  for i in range(0, len(csv_rows)): 
    formatted = createIndexSensitiveString(csv_rows[i], positions, maxLength)
    dssatFormattedRows.append(formatted)
  
  return dssatFormattedRows

def formatDFToDSSATTables(df, type):
  csv_rows = getFormattedDF(df)

  if type == 'TREATMENTS':         
    dssatFormattedRows = createDSSATFormatTable(csv_rows, gc('POS', 'TRTMNTS'))
  elif type == 'FIELD':            
    dssatFormattedRows = createDSSATFormatTable(csv_rows, gc('POS', 'FLD'))
  elif type == 'FIELD2':           
    dssatFormattedRows = createDSSATFormatTable(csv_rows, gc('POS', 'FLD_2'))
  elif type == 'PLANTING_DETAILS': 
    dssatFormattedRows = createDSSATFormatTable(csv_rows, gc('POS', 'PD'))

  return dssatFormattedRows

def wrapper(experimentFilesCount, cropCode, tsize=99): 
  cultivarLength = CULTIVAR_LENGTH[cropCode]
  code = CROP_FILE_EXT[cropCode][1:3]

  for i in range(1, experimentFilesCount + 1):
    name = 'UPLB18' + '0'*(2 - getDigitNum(i)) + str(i) + CROP_FILE_EXT[cropCode]
    if i == 1: 
      createExperimentTables(name, code, cultivarLength, tsize, True, True)
    else: 
      createExperimentTables(name, code, cultivarLength, tsize, True)

def createExperimentTables(experimentName, 
                           cropCode, 
                           cultivarLength, 
                           tsize, 
                           writeFlag=False, 
                           firstExpFlag=False): 

  EXPERIMENT_NAME = experimentName
  FIELD_SIZE = 99
  TREATMENTS_SIZE = tsize
  experimentCode = EXPERIMENT_NAME.split('.')[0]

  fieldIDPrefix = EXPERIMENT_NAME[0:4]
  fieldTable = createFieldDummyEntries(FIELD_SIZE, fieldIDPrefix, SOIL_LEVEL)
  fieldTable2 = createFieldTableTwo(FIELD_SIZE)
  treatmentTable = createTreatmentDummyEntries(TREATMENTS_SIZE, FIELD_SIZE, cultivarLength, firstExpFlag)

  chosenFieldStations = list(treatmentTable['FL'].values)
  chosenWeatherStations = fieldTable[fieldTable['@L'].isin(chosenFieldStations)][['@L', 'WSTA....']]

  plantingDetailsDF = createPlantingDetailsDummyEntries(chosenFieldStations, chosenWeatherStations)
  plantingDates = list(plantingDetailsDF['PDATE'])

  if writeFlag: 
    filepath = OUTPUT_PATH + cropCode + '_Experiments/' + EXPERIMENT_NAME

    with open(filepath, 'a') as fp: 
      startData = {
        'EXPERIMENT_CODE': experimentCode,
        'CROP_CODE': cropCode
      }

      treatmentFormatted = formatDFToDSSATTables(treatmentTable, 'TREATMENTS')
      fieldFormatted = formatDFToDSSATTables(fieldTable, 'FIELD')
      field2Formatted = formatDFToDSSATTables(fieldTable2, 'FIELD2')
      plantingDetailsFormatted = formatDFToDSSATTables(plantingDetailsDF, 'PLANTING_DETAILS')

      # Write header of DSSAT Experiment Files. 
      createExperimentSpecifics(fp, 'START', startData)
      writeTable(fp, treatmentFormatted, 1)
      appendCultivarTable(fp, cropCode)
      fp.write('*FIELDS\n')
      writeTable(fp, fieldFormatted, 0)
      writeTable(fp, field2Formatted, 1)
      fp.write('*PLANTING DETAILS\n')
      writeTable(fp, plantingDetailsFormatted, 1)
      fp.write('*SIMULATION CONTROLS\n')
      writeSimulationTables(fp, plantingDates)

if len(sys.argv) == 3: 
  try:
    experimentCount = int(sys.argv[1])
    crop = sys.argv[2].upper()
    if not crop in CULTIVAR_LENGTH: 
      print('Error: Crop code not found!')
    else: 
      wrapper(experimentCount, crop)
  except ValueError: 
    print('Error: Wrong Input')
    print('Usage: $ python createExperimentTables.py <experimentCount> <crop>')
else:
  print('Usage: $ python createExperimentTables.py <experimentCount> <crop>')
