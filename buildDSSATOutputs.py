import os
import pandas as pd
from io import StringIO

from helperFxns import(
  readFile, 
  pickleDictionary, 
  checkStringStart,
  checkSubstring,
  joinTokens
)

from experimentFileConstants import CROP_FILE_EXT

from parsingConfigs import(
  DSSAT_OUTPUT_PATH,
  OUTPUT_PATH,
  CLEANED_PLANTGRO_FILENAME,
  CLEANED_FILE_PATH
)

def createBufferStrings(rawContents):
  outputDict = {}
  treatmentNumber = None
  experimentCode = None
  tableFlag = False
  stringAccumulator = []
  COMMENT_SYMBOLS = ['!']
  WORD_ARRAY = ['DOY', 'DAS', 'DAP']

  for string in rawContents: 
    if((not string
        or checkStringStart(string, COMMENT_SYMBOLS))
       and tableFlag): 
      # Reset flags
      tableFlag = False
      if experimentCode and treatmentNumber: 
        outputDict[experimentCode][treatmentNumber] = joinTokens(stringAccumulator, '\n')
        treatmentNumber = experimentCode = None
        stringAccumulator.clear()
        continue

    if string.startswith('*RUN'): 
      tokens = string.split()
      treatmentNumber = tokens[-1]
      experimentCode = tokens[-2]
      if not experimentCode in outputDict: 
        outputDict[experimentCode] = {}
        continue

    if(string.startswith('@')
       and checkSubstring(string, WORD_ARRAY)):
      tableFlag = True

    if tableFlag: 
      tokens = string.split()
      stringAccumulator.append(joinTokens(tokens))

  if(len(stringAccumulator) > 0): 
    outputDict[experimentCode][treatmentNumber] = joinTokens(stringAccumulator, '\n')
  
  return outputDict

def buildPlantGro(cropName, writeFlag=False): 
  cropCode = CROP_FILE_EXT[cropName][1:3]
  contents = readFile(DSSAT_OUTPUT_PATH + cropCode + '_Outputs/PlantGro.OUT')
  processedStrings = createBufferStrings(contents)

  if writeFlag: 
    writePath = CLEANED_FILE_PATH + cropCode + CLEANED_PLANTGRO_FILENAME
    pickleDictionary(writePath, processedStrings, True)

  return processedStrings

# buildPlantGro(True)
