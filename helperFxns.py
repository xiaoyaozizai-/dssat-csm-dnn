import os
import json
import pprint
import pickle
import pandas as pd
import numpy as np

from io import StringIO

def dropColumns(dataframe, colList, exceptFlag=False):
  if exceptFlag: 
    return dataframe[colList]
  else: 
    dataframe.drop(colList, axis=1, inplace=True)
    return dataframe

def readStringCSV(stringCSV, separator=';', index=None): 
  '''
    Accepts a csv-formatted (';' separated) string and 
    returns a pandas dataframe. Index can be also specified 
    but the default value is None
  '''
  dataFrame = pd.read_csv(StringIO(stringCSV), index_col=index, sep=separator)

  return dataFrame

def joinTokens(tokens, sep=';'): 
  '''
    Joins the tokens with ';' as default separator. 
  '''
  return sep.join(tokens)

def countEndString(inputDict, string): 
  '''
    Returns the number of strings that end with 'string' within in the values of dict.
    Input: 
      dict - dictionary. 
  '''
  return sum([1 for x in list(inputDict.values()) if x.endswith(string)])

def checkStringStart(string, symbolList): 
  '''
    Checks if a string starts with symbol in symbolList
    Returns True if it is. False otherwise. 
  '''
  for symbol in symbolList: 
    if string.startswith(symbol): return True
  return False

def mapListToNewDictValues(valueList): 
  '''
    Creates a dictionary with integers as keys and valueList as values. 
    So it returns a dictionary with size len(valueList)
  '''
  return {k : v for (k, v) 
          in zip(np.arange(len(valueList)), valueList)}

def mapListToNewDictKeys(keysList, val): 
  '''
    Creates a dictionary that contains the keys from 'valueList' 
    and 'val' as value
  '''
  return {x:val for x in keysList}

def checkDictionaryValues(dictionary, val, needLen=False):
  '''
    Finds the key/s where val is equal to values in dictionary.
    Input: 
      dictionary - 
      val - value to be compared with. 
      needLen(default: False) - if the length of value list array is needed instead of the keys. 
  '''
  keyList = [k for k, v in dictionary.items() if v == val]
  if needLen: 
    return len(keyList)
  else: 
    return keyList

def checkSubstring(string, wordArray, ormode=False):
  '''
    Returns true if all the elements in the wordArray exist in the string. 
    Input: 
      - string
      - wordArray - the words to be checked with the string
      - ormode(default: False) - if true and one word from the wordArray exist in the string, return true.
  '''
  
  if(not string): return False
  string = string.lower()

  if ormode: 
    for word in wordArray: 
      word = word.lower()
      if word in string: return True
    return False
  else: 
    for word in wordArray:
      word = word.lower()
      if not word in string: return False
    return True

def createDirectory(filePath): 
  path = filePath.split('/')
  path = '/'.join(path[:len(path) - 1])

  if(not os.path.isdir(path)): 
    os.mkdir(path)

def writeToFile(
  filePath, 
  contents, 
  writeMode='w',
  pprnt=False, 
  new=False, 
  close=False,
): 
  '''
    Writes contents to filepath
    Input: 
      - filepath - string (filepath + filename)
      - contents - things to write. 
      - pprnt - applies pprint formatting. 
      - new (default:False) - if new file. Checks the directory if it exist. 
      - close (default:False) - flag for closing the file. 
  '''
  if(new):
    createDirectory(filePath)
  
  with open(filePath, writeMode) as fileWrite:
    if(type(contents) is dict):
      if pprnt: pprint.pprint(contents, fileWrite, width=200)
      else: fileWrite.write(json.dumps(contents, indent=2))
    else: 
      if pprnt: pprint.pprint(contents, fileWrite, width=200)
      else: fileWrite.write(contents)
    
  if close: fileWrite.close()

def pickleDictionary(filePath, contents, new=False): 
  '''
    Writes contents to filepath
    Input: 
      - filepath - string (filepath + filename)
      - contents - things to write. 
      - new (default:False) - if new file. Checks the directory if it exist. 
  '''
  if(new):
    createDirectory(filePath)
  
  with open(filePath, 'wb') as fp:
    pickle.dump(contents, fp)

def loadPickle(filePath): 
  with open(filePath, 'rb') as fp: 
    data = pickle.load(fp)
    return data

def readFile(filePath, rstrip=True, latinEncoder=True): 
  '''
    Input:
      - filePath (String)
    Output:
      - content (List of Strings)
  '''
  if latinEncoder: 
    with open(filePath, 'r', encoding='latin-1',) as fileRead:
      content = fileRead.readlines()
  else: 
    with open(filePath, 'r') as fileRead:
      content = fileRead.readlines()

  if rstrip: content = [x.rstrip() for x in content]
  else: content = [x.strip() for x in content]
  
  fileRead.close()

  return content
