# For checking if all the data are properly parsed. 

import pprint
import re

from helperFxns import readFile, checkSubstring
from parsingConfigs import EXCLUDE_SOIL_TABLE

codeDictionary = {}
pp = pprint.PrettyPrinter(width=200)

def checkSoilTables(filename, codeDict, codeList): 
  sizeCD = len(codeDict)
  sizeCL = len(codeList)
  
  if(sizeCD != sizeCL): 
    print(
      'Size not Match! (' + filename + ') -> CountedSize: ' 
      + str(sizeCD) + ' ParsedSize: ' + str(sizeCL)
    )

  for code in codeList:
    if code not in codeDict: 
      print('Code Not Found (' + filename + '): ' + code)
      return False
    else: 
      codeDict[code] = True

  if(sizeCD != sizeCL): 
    pp.pprint(codeDict.items())
  else: 
    print('Codes Complete! (' + filename + ')')
    
  return True

def countSoilTables(filename, contents): 
  counter = 0
  wordArray = ['*SOIL', '!']
  
  # String starts with * followed by space. 
  regex = '^\*\s+'
  regexObj = re.compile(regex)

  for string in contents: 
    if(string 
       and '*' in string 
       and not regexObj.match(string) 
       and not checkSubstring(string, EXCLUDE_SOIL_TABLE) 
       and not checkSubstring(string, wordArray, True)): 

      soilCode = string.split()[0]
      codeDictionary[soilCode] = False
      counter += 1

  return codeDictionary