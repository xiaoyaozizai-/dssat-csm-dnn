import os
import os.path
import pprint 
import pandas as pd

from io import StringIO


'''
  TODO:  
    - April 30 - Please use createTrainingData.py instead. 
    - Things to do. 
      - One Hot Encode categorical variables. 
        - Cultivars - CR, INGENO - Can't be null. 
        - Planting Conditions - PLME, PLDS - 
        - Soil General Info - SMHB, SMPX, SMKE, SCOM
'''

from helperFxns import (
  readFile,
  dropColumns,
  readStringCSV,
  mapListToNewDictValues
)

from parsingConfigs import (
  EXPERIMENT_READ_PATH, 
  OUTPUT_PATH, 
  CLEANED_FILE_PATH,
  CLEANED_WEATHER_FILENAME,
  CLEANED_SOIL_FILENAME
)

from buildExperimentDictionary import buildExperimentDictionary
from buildWeatherDictionary import buildWeatherDict
from buildPlantingDuration import buildPlantingDuration
from buildGenotypeDict import buildGenotypeDict
from buildSoilDictionary import fastDataBuild
from buildDSSATOutputs import buildPlantGro

soilDict = fastDataBuild()
plantGroOutputs = buildPlantGro()
experimentDict = buildExperimentDictionary()
plantingDuration = buildPlantingDuration()
cultivarMappings = buildGenotypeDict()

# Constants
DAILYVAL_KEY    = 'weather_DailyValues'
GENINFO_KEY     = 'weather_GeneralInfo'
SOILLEVELS_KEY  = 'soil_LevelData'
SOILGENINFO_KEY = 'soil_GeneralInfo'
TEST_FILENAME   = 'NCRM8701.SBX'

# Pretty-printer Configs
pp = pprint.PrettyPrinter(width=200)
pd.set_option('display.expand_frame_repr', False)

CATEGORICAL_VARS = ['CR', 'INGENO', 'PLME', 'PLDS', 'SMHB', 'SMPX', 'SMKE', 'SCOM']

COLS_TO_BE_DROPPED = {
  'TRTMNTS': ['R', 'O', 'C'],
  'WTH_DAYINFO': ['DATE']
}

RETAIN_THESE_VARS = {
  'PLNTGS': ['PPOE', 'PLME', 'PLDS', 'PLRS', 'PLRD', 'PLDP','PLWT'],
  'WTH_GENINFO': ['LAT', 'LONG', 'ELEV'],
  'WTH_DAYINFO': ['SRAD', 'TMAX', 'TMIN', 'RAIN'],
  'SOIL_LVL': ['SLB', 'SLLL', 'SDUL', 'SSAT', 'SRGF', 'SBDM', 'SLOC',
               'SLHW', 'SLCL', 'SLSI', 'SLNI' ], 
}

OUTPUTS_TO_BE_EXTRACTED = {
  'PLANTGRO': ['PWAD', 'DAP']
}

TABLE_INDICES = {
  'CLTVR': ['C'],
  'ENVRNMNT': [],
  'FRTLZRS': [],
  'FLDS': ['L', 'L'],
  'INTIAL': [],
  'IRRIGTN': [],
  'RSDUES': [],
  'TLLGE': [],
  'TRTMNTS': [],
  'PLNTGS': ['P'],
  'HRVST': [],
}

def createCommonDF(bufferString, indexString=None): 
  # For segments that only contains one table. 
    return readStringCSV(bufferString, index=indexString) 

def mergeDatasets(dataframesList, resetIndex=True, axs=1): 
  for df in dataframesList:
    # drop the indices
    df.reset_index(inplace=True, drop=resetIndex)
  
  return pd.concat(dataframesList, axis=axs)

def getOutputs(experimentCode, treatmentCode, columns=[]): 
  bufferString = plantGroOutputs[experimentCode][treatmentCode]
  df = createCommonDF(bufferString)
  
  return df[columns]

def oneHotEncode(trainingDataDict): 
  for key in trainingDataDict:
    # fill nans of categorical variables. 
    df = trainingDataDict[key][CATEGORICAL_VARS].fillna('NANSTR')
    # Generate dummy columns for the variables. 
    dummyDfs = pd.get_dummies(df)
    # Remove the categorical variables and merge the encoded variables to
    # training data.
    trainingDataDict[key] = dropColumns(trainingDataDict[key], CATEGORICAL_VARS)
    trainingDataDict[key] = mergeDatasets([trainingDataDict[key], dummyDfs])

  return trainingDataDict

def buildExperiment(experimentCode, dfCaches, trainingDataDict):
  trainingDataDictCopy = trainingDataDict.copy()
  weatherDailyValDF = dfCaches[DAILYVAL_KEY].copy()
  weatherGenInfoDF  = dfCaches[GENINFO_KEY].copy()
  soilLevelsData = dfCaches[SOILLEVELS_KEY].copy()
  soilGenInfoData = dfCaches[SOILGENINFO_KEY].copy()

  dataframesDict = {}
  experiment = experimentDict[experimentCode]

  # Create dataframes out of the bufferStrings. 
  for key, val in experiment.items():
    dataframesDict[key] = {}
    for c in val:  
      if len(TABLE_INDICES[key]) > 0:
        dataframesDict[key][c] = createCommonDF(experiment[key][c], 
                                                TABLE_INDICES[key][c]) 
      else: 
        dataframesDict[key][c] = createCommonDF(experiment[key][c])

  # Build the training data based on the configs of 'TRTMNTS' table. 
  treatmentsDF = dataframesDict['TRTMNTS'][0]
  fieldsDF = dataframesDict['FLDS'][0]
  plantingCondDF = dataframesDict['PLNTGS'][0]
  cultivarDF = dataframesDict['CLTVR'][0]

  # Retain relevant variables. 
  plantingCondDF = dropColumns(plantingCondDF, 
                               RETAIN_THESE_VARS['PLNTGS'], True)

  for row in treatmentsDF.itertuples():
    treatmentLevel = getattr(row, 'N')
    outputs = getOutputs(experimentCode, 
                         str(treatmentLevel), 
                         OUTPUTS_TO_BE_EXTRACTED['PLANTGRO'])

    if getattr(row, 'FL') != 0: 
      fieldLevel = getattr(row, 'FL')
      plantingCondLevel = getattr(row, 'MP')
      cultivarLevel = getattr(row, 'CU')

      # Get weather station code and soil ID.  
      weatherStation = fieldsDF.at[fieldLevel, 'WSTA']
      soilID = fieldsDF.at[fieldLevel, 'ID_SOIL']

      plantingRange = plantingDuration[experimentCode][str(treatmentLevel)]
      plantingStart = int(plantingRange['START'])

      if len(weatherStation) == 4: 
        plantingYear = str(plantingStart)[0:2] + '01'
        weatherStation += plantingYear

      # Cache Soil Data. 
      if not soilID in soilLevelsData:
        if soilID in soilDict: 
          genInfoBufferString = soilDict[soilID]['SOIL_GENERAL_INFO']
          soilLeveloneBufferString = soilDict[soilID]['SOIL_LEVEL_ONE']
          soilLevelsData[soilID] = createCommonDF(soilLeveloneBufferString)
          soilGenInfoData[soilID] = createCommonDF(genInfoBufferString)
        else: 
          print('%s not found!' % soilID)
      
      # Cache Weather Data. 
      if(not weatherStation in weatherDailyValDF
         and not weatherStation in weatherGenInfoDF):
        data = buildWeatherDict(False, weatherStation)
        dailyValBufferString = data['WEATHER_DAILY_VALUES']
        genInfoBufferString = data['WEATHER_GENERAL_INFO']

        weatherDailyValDF[weatherStation] = createCommonDF(dailyValBufferString)
        weatherGenInfoDF[weatherStation] = createCommonDF(genInfoBufferString)

      soilDF = soilLevelsData[soilID]
      weatherDF = weatherDailyValDF[weatherStation]
      soilGenInfoDF = soilGenInfoData[soilID]
      wthGenInfoDF = weatherGenInfoDF[weatherStation]

      # Select weather rows based on the outputs. 
      days = list(outputs['DAP'].values)
      rows = [plantingStart + x for x in days]

      weatherRows = weatherDF[weatherDF['DATE'].isin(rows)]

      # Retain relevant variables
      weatherRows = dropColumns(weatherRows, RETAIN_THESE_VARS['WTH_DAYINFO'], True)      
      wthGenInfoDF = dropColumns(wthGenInfoDF, RETAIN_THESE_VARS['WTH_GENINFO'], True)
      soilDF = dropColumns(soilDF, RETAIN_THESE_VARS['SOIL_LVL'], True)

      # Flatten the soil data. 
      flattenedSoilDF = soilDF.values.flatten()

      numSoilLevel = soilDF.shape[0]
      trainingDataSize = outputs.shape[0]
      soilDataRows = pd.DataFrame([flattenedSoilDF] * trainingDataSize)
      cultivarRows = pd.DataFrame([cultivarDF.loc[cultivarLevel]] * trainingDataSize)
      plantingRows = pd.DataFrame([plantingCondDF.loc[plantingCondLevel]] * trainingDataSize)
      soilGenInfoRows = pd.DataFrame([soilGenInfoDF.loc[0]] * trainingDataSize)
      weatherGenInfoRows = pd.DataFrame([wthGenInfoDF.loc[0]] * trainingDataSize)

      data = mergeDatasets([cultivarRows,
                            plantingRows, 
                            weatherGenInfoRows,
                            weatherRows,
                            soilGenInfoRows,
                            soilDataRows, 
                            outputs])
      
      if not numSoilLevel in trainingDataDictCopy: 
        trainingDataDictCopy[numSoilLevel] = pd.DataFrame()

      trainingDataDictCopy[numSoilLevel] = trainingDataDictCopy[numSoilLevel].append(data, ignore_index=True)

  dfCache = {
    DAILYVAL_KEY: weatherDailyValDF,
    GENINFO_KEY: weatherGenInfoDF,
    SOILLEVELS_KEY: {},
    SOILGENINFO_KEY: {}
  }

  return [trainingDataDictCopy, dfCache]

def buildTrainingData(writeFlag=False):
  dfCaches = {
    DAILYVAL_KEY: {},
    GENINFO_KEY: {},
    SOILLEVELS_KEY: {},
    SOILGENINFO_KEY: {}
  }

  trainingDataDict = {}

  for filename in os.listdir(EXPERIMENT_READ_PATH): 
    experimentCode = filename.split('.')[0]
    if filename.endswith('.SBX'):
      [trainingDataDict, dfCaches] = buildExperiment(experimentCode, 
                                                     dfCaches, 
                                                     trainingDataDict)

  # One Hot Encode categorical variables.
  trainingDataDict = oneHotEncode(trainingDataDict)

  if writeFlag: 
    for key in trainingDataDict: 
      name = 'trainingData_' + str(key) + '_SLS.csv'
      trainingDataDict[key].to_csv(OUTPUT_PATH + name, index=False)

# buildTrainingData()
# buildTrainingData(True)
