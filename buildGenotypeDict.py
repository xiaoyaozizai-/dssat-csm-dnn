import os
import pprint

'''
  SALUS046 - Has different format from other cultivar files. 
           - [FIXED] Created special Condition for this. 
  CSCAS046 - 5 Rows with the same cultivar codes and values. 
           - [NOTE] Harmless. Won't fix unless I need it 
  MZCER046 - [NOTE] 2 Rows with similar cultivar code. Different Values. 
    IB0067 PIO 3324 . IB0001 320.0 0.520 940.0 625.0  6.00 38.90
    IB0067 TEST     . IB0001 130.0 0.500 720.0 380.0  7.50 75.00
  BMGRO046 - [NOTE] 2 Rows with similar cultivar codes. Belongs in different ecotypes. 
'''

from helperFxns import (
  readFile, 
  writeToFile, 
  checkStringStart,
  checkSubstring, 
  mapListToNewDictKeys,
  mapListToNewDictValues
)

from parsingConfigs import (
  DSSAT_GENOTYPES_READ_PATH, 
  OUTPUT_PATH, 
  DSSAT_GENOTYPES_OUTPUT_NAME
)

def processFile(rawContents, code): 
  EXCLUDE_THIS = ['!', '*', '@', '$', '\x1a']
  cultivarCodes = []

  for string in rawContents: 
    if(string and not checkStringStart(string, EXCLUDE_THIS)):
      cultivarCode = ''

      if code == 'SALUS046': cultivarCode = string.split()[1]
      else: cultivarCode = string[:6].strip()
      
      cultivarCodes.append(cultivarCode)

  # Remove duplicates
  tokens = list(dict.fromkeys(cultivarCodes))
  return dict(zip(cultivarCodes, range(1, len(tokens) + 1)))

def buildGenotypeDict(writeFlag=False): 
  genotypes = {}
  writePath = OUTPUT_PATH + DSSAT_GENOTYPES_OUTPUT_NAME
  
  for filename in os.listdir(DSSAT_GENOTYPES_READ_PATH): 
    if filename.endswith('.CUL'): 
      code = filename.split('.')[0]
      contents = readFile(DSSAT_GENOTYPES_READ_PATH + filename)
      genotypes[code] = processFile(contents, code)

  if writeFlag: 
    writeToFile(writePath, genotypes, 'w', True, True, True)

  return genotypes

# buildGenotypeDict(True)