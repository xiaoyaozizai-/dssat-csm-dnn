import os
import re
import pprint
import pandas as pd
from io import StringIO

from helperFxns import (
  readFile, 
  joinTokens,
  pickleDictionary,
  checkSubstring,
  mapListToNewDictKeys
)

from experimentFileConstants import CROP_FILE_EXT
from parsingConfigs import (
  OUTPUT_PATH,
  TABLE_MAPPINGS,
  EXPERIMENT_READ_PATH,
  CLEANED_FILE_PATH,
  CLEANED_EXPERIMENT_FILENAME
)

REMOVE_THIS = '[@\.\*]'
EXCLUDE_THIS = ['*EXP.', 'GENERAL']
REMOVE_LAST_COLS = {
  'FLDS': ['FLNAME', 'XCRD'],
  'INTIAL': ['ICNAME', 'SH20'],
  'IRRIGTN': ['IRNAME', 'IDATE'],
  'PLNTGS': ['PLNAME', ''],
  'HRVST': ['HNAME', ''],
  'ENVRNMNT': ['ENVNAME', ''],
  'FRTLZRS': ['FERNAME', ''],
  'RSDUES': ['RENAME', ''],
  'TLLGE': ['TNAME', '']
}

GLOBAL_VARS = {
  'tokenLength': 0,
  'tokenFlag': False,
}

regexObj = re.compile(REMOVE_THIS)

def testforParserError(data): 
  for key, val in data.items(): 
    for k, v in val.items():
      for c in v: 
        try:
          pd.read_csv(StringIO(v[c]), sep=';')
        except pd.errors.ParserError:
          print(k)

def removeName(currentHeader, tokens):
  '''
    Removes name columns in tables. 
    Input: 
      - currentHeader - Identifier for the current table. 
      - tokens - cleaned tokens
  '''

  lastColKeys = list(REMOVE_LAST_COLS.keys())

  if currentHeader == 'TRTMNTS': 
    return tokens[0:4] + tokens[-13:]
  elif currentHeader == 'CLTVR': 
    return tokens[0:3]
  elif (currentHeader in lastColKeys):
    colList = REMOVE_LAST_COLS[currentHeader]
    if colList[1] and colList[1] in tokens:
      # Stopping condition if the section contains 2 tables. 
      GLOBAL_VARS['tokenFlag'] = False
      GLOBAL_VARS['tokenLength'] = 0

    if colList[0] in tokens: 
      GLOBAL_VARS['tokenLength'] = len(tokens) - 1
      GLOBAL_VARS['tokenFlag'] = True
      # Remove the last element. 
      return tokens[:-1]
    else: 
      if GLOBAL_VARS['tokenFlag']: 
        return tokens[0: GLOBAL_VARS['tokenLength']]
  
  return tokens

def cleanFile(rawContents):
  currentHeader = ''
  stringAccumulator = []
  sections = {}
  startHeader = True
  count = 0
  NULL_SIGNS  = ['-99']

  for string in rawContents: 
    # Stopping Condition
    if not string or string.startswith('!'): 
      if len(stringAccumulator) > 1:
        sections[currentHeader][count] = joinTokens(stringAccumulator, '\n')
        stringAccumulator.clear()
        currentHeader = ''
        continue
    
    # For catching headers. 
    if(not currentHeader
       and string.startswith('*') 
       and not checkSubstring(string, EXCLUDE_THIS)): 
      tablename = regexObj.sub('', string).split()[0]

      if tablename in TABLE_MAPPINGS: 
        currentHeader = TABLE_MAPPINGS[tablename]
        # Reset Flags
        sections[currentHeader] = {}
        startHeader = True
        GLOBAL_VARS['tokenLength'] = 0
        GLOBAL_VARS['tokenFlag'] = False
        count = 0
        continue
    
    if currentHeader:
      if string.startswith('@'): 
        if startHeader: 
          startHeader = False
        else: 
          sections[currentHeader][count] = joinTokens(stringAccumulator, '\n')
          stringAccumulator.clear()
          count += 1

      tokens = regexObj.sub('', string).split() 
      tokens = removeName(currentHeader, tokens) 
      tokens = [x if not checkSubstring(x, NULL_SIGNS, True) else '' for x in tokens]
      stringAccumulator.append(joinTokens(tokens))
  
  return sections

def buildExperimentDictionary(cropName='', writeFlag=True, filename=None): 
  experiments = {}
  if filename: 
    cropCode = filename.split('.')[1][0:2]
    readPath = EXPERIMENT_READ_PATH + cropCode + '_Experiments/'
    if filename.upper() in os.listdir(readPath): 
      contents = readFile(readPath + filename)
      name = filename.split('.')[0]
      experiments[name] = cleanFile(contents)
  else:
    if not cropName: 
      print('Error: Provide crop name to build experiment dictionary!')
    else: 
      cropExt = CROP_FILE_EXT[cropName]
      cropCode = cropExt[1:3]
      readPath = EXPERIMENT_READ_PATH + cropCode + '_Experiments/'

      for filename in os.listdir(readPath): 
        if filename.upper().endswith(cropExt): 
          contents = readFile(readPath + filename)
          name = filename.split('.')[0]
          experiments[name] = cleanFile(contents)
    
    if writeFlag: 
      writePath = CLEANED_FILE_PATH + cropCode + CLEANED_EXPERIMENT_FILENAME
      pickleDictionary(writePath, experiments, True)

  return experiments

# buildExperimentDictionary(True)
# buildExperimentDictionary()
