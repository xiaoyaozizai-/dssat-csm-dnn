# Approximating DSSAT-CSM Growth Outputs using Deep Neural Networks #

An attempt to create regression models (using DNN) for the growth parameters of the crops Soybean, Peanut and Tomato. The datasets for each crop were created out of the inputs and simulated outputs of Decision Support System for Agrotechnology Transfer (DSSAT).

## Setup ##

* DSSAT Version - 4.6
* Keras and Tensorflow 
* The models were trained on a single NVIDIA Tesla K80 via Google Collaboratory. 

## Contact ##

* Brian Joshua M. Revilla (bjmrevilla@gmail.com) - researcher
* Jaderick Pabico - research adviser