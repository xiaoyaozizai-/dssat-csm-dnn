# For creating inputs to be predicted by the trained model. 
# Requires an unshuffled training data

import pandas as pd

from parsingConfigs import (
  CLEANED_FILE_PATH, 
  TRAINED_DNN_TEST_INPUTS_FP
)

def createSampleInputs(filename='SB_TrainingData.gz'): 
  counter = 0
  outputDict = {}
  fp = CLEANED_FILE_PATH + filename

  df = pd.read_pickle(fp)
  indices = df.index[df['DAP'] == 0].tolist()

  for i in range(0, len(indices) - 1):
    outputDict[counter] = df.iloc[indices[i]:indices[i + 1] - 1]
    counter += 1


  for key, val in outputDict.items(): 
    print('Processing ' + str(key) + '....')
    fp = TRAINED_DNN_TEST_INPUTS_FP + str(key) + '_testInput.gz'
    val.to_pickle(fp)

createSampleInputs()
