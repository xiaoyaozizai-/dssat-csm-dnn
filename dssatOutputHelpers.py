import os
import pprint

pp = pprint.PrettyPrinter(width=200)

from parsingConfigs import (
  DSSAT_OUTPUT_PATH
)

from helperFxns import (
  readFile
)

def isNumber(s): 
  try: 
    int(s)
    return True
  except ValueError: 
    return False

def determineGoodTreatments(cropCode): 
  goodResults = {}
  filepath = DSSAT_OUTPUT_PATH + cropCode + '_Outputs/Summary.OUT'
  contents = readFile(filepath)

  for string in contents: 
    if string: 
      tokens = string.split()
      nullCount = string.count('-99')
      if nullCount < 13 and isNumber(tokens[0]): 
        goodResults[tokens[0]] = False
      
  return goodResults

# determineGoodTreatments()
