import os
import pandas as pd
import numpy as np 

from parsingConfigs import (
  OUTPUT_PATH
)

from helperFxns import (
  joinTokens
)

'''
  Assumptions
    - Variables that are randomized. 
      - SCOM - values listed
      - SLDR - values listed based on type. 
      - SLRO - values listed based on type. 
      - SLPF - ratio, 0 - 1

      - SLSI* - 0 - 100 
      - SLCL* - 0 - 100
      - SLCF - 0 - 100
      - SLOC - 0 - 58
      - SLNI - 0 - 20

    
    - * the sum of SLSI and SLCL can't be more than 100

'''

def getDigitNum(x): 
  if x > 999: return 4
  elif x > 99: return 3
  elif x > 9: return 2
  elif x > 0: return 1
  else: return 0

def createIndexSensitiveString(string, positions, maxLength, tokens=[], sep=','): 
  '''
    - Changes the position of the tokens based on the given positions.
    - arguments
      - string - string to be processed. 
      - positions - index positions based on the header. 
  '''

  if len(tokens) == 0: 
    tokens = string.split(sep)
    tokens = [token.strip() for token in tokens]

  # Create string with the same length as the string. 
  if maxLength > len(string): 
    tempString = ' ' * maxLength
  else: 
    tempString = ' ' * len(string)
    
  # Append the tokens based on position. 
  for i in range(0, len(tokens)): 
    tempString = tempString[:positions[i]] + tokens[i] + tempString[positions[i]:]

  return tempString.rstrip()

def getConstantString(type, key): 
  CONSTANT_STRINGS = {
    'POS': {
      'H_1': [0, 13, 25, 36, 39],
      'H_2': [1, 13, 30, 38, 43, 47],
      'H_3': [2, 8, 14, 20, 26, 32, 38, 44, 50, 56],
      'H_4': [3, 8, 14, 20, 26, 32, 38, 44, 50, 56, 62, 68, 74, 80, 86, 92, 98],
    },
    'STR': {
      'H_1': '*UPLBSP0001  BJMREVILLA  S          5  Soil Randomly Generated 001',
      'H_2': '@SITE        COUNTRY          LAT     LONG SCS FAMILY',
      'H_3': '@ SCOM  SALB  SLU1  SLDR  SLRO  SLNF  SLPF  SMHB  SMPX  SMKE',
      'H_4': '@  SLB  SLMH  SLLL  SDUL  SSAT  SRGF  SSKS  SBDM  SLOC  SLCL  SLSI  SLCF  SLNI  SLHW  SLHB  SCEC  SADC'
    },
    'TOKENS': {
      'H_1': ['*UPLBSP0001', 'BJMREVILLA', 'S', '5', 'Soil Randomly Generated 001'],
      'H_2': ['@SITE', 'COUNTRY', 'LAT', 'LONG', 'SCS FAMILY'],
      'H_3': ['@ SCOM', 'SALB', 'SLU1', 'SLDR', 'SLRO', 'SLNF', 'SLPF', 'SMHB', 'SMPX', 'SMKE'],
      'H_4': ['@  SLB','SLMH', 'SLLL', 'SDUL', 'SSAT', 'SRGF', 'SSKS', 'SBDM', 
              'SLOC', 'SLCL', 'SLSI', 'SLCF', 'SLNI', 'SLHW', 'SLHB', 'SCEC', 'SADC']
    },
    'INPUT': {
      'SI_2': ['LB', 'PH', '-99', '-99', 'No Classification'],
      'SI_3': ['<SCOM>', '<SALB>', '-99', '<SLDR>', '<SLRO>', '-99', '<SLPF>', 'IB001', 'IB001', 'IB001'],
      'SI_4': ['<SLB>', '-99', '-99', '-99', '-99', '-99', '-99', '-99', '<SLOC>', '<SLCL>', '<SLSI>', 
               '<SLCF>', '<SLNI>', '<SLHW>', '-99', '<SCEC>', '-99']
    },
    'CHOICES': {
      'SCOM': ['BN', 'R', 'BL', 'G', 'Y'], 
      'SALB': ['0.13', '0.14', '0.09', '0.13', '0.17'],
      'SLDR': ['0.95', '0.85', '0.75', '0.6', '0.4', '0.25', '0.05', '0.01'],
      'SLRO': ['94', '91', '83', '71'],
    }
  }

  return CONSTANT_STRINGS[type][key]

def createHeaderOne(i): 
  dummyRow = getConstantString('TOKENS', 'H_1').copy()
  dummyRow[0] = '*UPLBSP' + '0' * (4 - getDigitNum(i)) + str(i)
  dummyRow[-1] = 'Randomly Generated Soil ' + '0' * (3 - getDigitNum(i)) + str(i)

  return createIndexSensitiveString(getConstantString('STR', 'H_1'), 
                                    getConstantString('POS', 'H_1'), 
                                    len(getConstantString('STR', 'H_1')), 
                                    dummyRow)

def createTableTwoRow(): 
  return createIndexSensitiveString(getConstantString('STR', 'H_2'), 
                                    getConstantString('POS', 'H_2'), 
                                    len(getConstantString('STR', 'H_2')), 
                                    getConstantString('INPUT', 'SI_2').copy())  

def createTableThreeRow(): 
  dummyRow = getConstantString('INPUT', 'SI_3').copy()
  dummySTR = getConstantString('STR', 'H_3')

  scom_list = getConstantString('CHOICES', 'SCOM')
  salb_list = getConstantString('CHOICES', 'SALB')
  sldr_list = getConstantString('CHOICES', 'SLDR')
  slro_list = getConstantString('CHOICES', 'SLRO')

  SCOM_r = np.random.randint(0, len(scom_list))
  SLDR_r = np.random.randint(0, len(sldr_list))
  SLRO_r = np.random.randint(0, len(slro_list))

  dummyRow[0] = scom_list[SCOM_r]
  dummyRow[1] = salb_list[SCOM_r]
  dummyRow[3] = sldr_list[SLDR_r]
  dummyRow[4] = slro_list[SLRO_r]
  dummyRow[6] = '%0.2f' % np.random.uniform(0, 1)

  return createIndexSensitiveString(getConstantString('STR', 'H_3'), 
                                    getConstantString('POS', 'H_3'), 
                                    len(getConstantString('STR', 'H_3')), 
                                    dummyRow)  

def createSoilLevelData(fp, depthList): 
  for i in range(0, len(depthList)): 
    SLSI_r = np.random.randint(0, 100)
    SLCL_r = np.random.randint(0, 100 - SLSI_r)

    dummyRow = getConstantString('INPUT', 'SI_4').copy()
    dummyRow[0] = str(depthList[i])
    dummyRow[8] = str(np.random.randint(0, 58))
    dummyRow[9] = str(SLCL_r)
    dummyRow[10] = str(SLSI_r)
    dummyRow[11] = str(np.random.randint(0, 100))
    dummyRow[12] = str(np.random.randint(0, 20))
    dummyRow[13] = str(np.random.randint(4, 8))
    dummyRow[15] = '%.2f' % np.random.uniform(0, 100) 

    dstr = createIndexSensitiveString(getConstantString('STR', 'H_4'), 
                                      getConstantString('POS', 'H_4'), 
                                      len(getConstantString('STR', 'H_4')), 
                                      dummyRow)

    fp.write(dstr + '\n')

def createSoilTable(num): 
  SOIL_LEVELS = 6
  SOIL_DEPTH_RANGE = [3, 200]

  with open(OUTPUT_PATH + 'customSoilFile.txt', 'w') as fp: 
    fp.write('*SOILS: General DSSAT Soil Input File\n\n')

    for i in range(0, num): 
      soilDepths_random = np.random.choice(SOIL_DEPTH_RANGE[1], SOIL_LEVELS, replace=False)
      soilDepths_random.sort()

      fp.write(createHeaderOne(i + 1) + '\n')
      fp.write(getConstantString('STR', 'H_2') + '\n')
      fp.write(createTableTwoRow() + '\n')
      fp.write(getConstantString('STR', 'H_3') + '\n')
      fp.write(createTableThreeRow() + '\n')
      fp.write(getConstantString('STR', 'H_4') + '\n')
      createSoilLevelData(fp, soilDepths_random)

      fp.write('\n')

createSoilTable(500)
