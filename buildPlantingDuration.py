import os
import pprint

from helperFxns import (
  readFile,
  pickleDictionary,
  checkSubstring,
)

from dssatOutputHelpers import (
  determineGoodTreatments
)

from experimentFileConstants import CROP_FILE_EXT
from parsingConfigs import (
  DSSAT_OUTPUT_PATH,
  OUTPUT_PATH,
  CLEANED_FILE_PATH,
  CLEANED_PLANTING_DURTN_FILENAME
)

pp = pprint.PrettyPrinter(width=200)

def getPlantDuration(rawContents, cropCode): 
  tableFlag = False
  experimentCode = ''
  treatmentNumber = ''
  outputDict = {}
  dummyDict = {}
  WORD_ARRAY = ['YEAR', 'DOY', 'DAS', 'DAP']

  excludeFlag = False
  goodSimulations = determineGoodTreatments(cropCode)

  for x in range(0, len(rawContents)): 
    if not rawContents[x]: continue

    if rawContents[x].startswith('*RUN'): 
      excludeFlag = False
      tokens = rawContents[x].split()
      runNumber = tokens[1]

      if str(runNumber) in goodSimulations: 
        treatmentNumber = tokens[-1]
        experimentCode = tokens[-2]

        if not experimentCode in outputDict: 
          outputDict[experimentCode] = {}
      else: 
        excludeFlag = True

    # Header found
    if not excludeFlag: 
      if not tableFlag:
        if checkSubstring(rawContents[x], WORD_ARRAY):
          dummyDict.clear()
          tokens = rawContents[x + 1].split()
          # Get the year and DOY (Date of Year)
          dummyDict['START'] = tokens[0][-2:] + tokens[1]
          tableFlag = True
      else:
        error = True
        try:
          if not rawContents[x + 1]: error = False
        except IndexError: error = False

        if not error: 
          tokens = rawContents[x].split()
          dummyDict['END'] = tokens[0][-2:] + tokens[1]
          if not treatmentNumber in outputDict[experimentCode]: 
            outputDict[experimentCode][treatmentNumber] = dummyDict.copy()
          tableFlag = False

  return outputDict

def buildPlantingDuration(cropName, writeFlag=False): 
  cropCode = CROP_FILE_EXT[cropName][1:3]
  readPath = DSSAT_OUTPUT_PATH + cropCode + '_Outputs/PlantC.OUT'
  contents = readFile(readPath, False)
  plantingDuration = getPlantDuration(contents, cropCode)

  if writeFlag: 
    writePath = CLEANED_FILE_PATH + cropCode + CLEANED_PLANTING_DURTN_FILENAME
    pickleDictionary(writePath, plantingDuration, True)

  return plantingDuration

# buildPlantingDuration(True)
# buildPlantingDuration()
