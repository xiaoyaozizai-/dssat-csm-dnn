import os
import re
import pprint

from parsingConfigs import (
  DSSAT_OUTPUT_PATH,
  OUTPUT_PATH
)

from helperFxns import (
  readFile, 
  writeToFile,
  joinTokens
)

from genHeaderMeanings import (
  parseMeaningDictionary,
  createHeaderMeaningDict
)

REGEX_REMOVE_THIS = '[@\.]'
regexObj = re.compile(REGEX_REMOVE_THIS)

def cleanFile(rawContents, filename):
  outputDict = {}

  for string in rawContents: 
    if string.startswith('@'): 
      tokens = regexObj.sub('', string).split()
      for token in tokens: 
        if not token in outputDict: 
          outputDict[token] = filename

  return outputDict

def genDSSATOutputHeaderMeanings(writeFlag=False):
  output = {}
  outputFilePath = OUTPUT_PATH + 'dssatOutputHeaderMeanings.txt'
  meanings = parseMeaningDictionary(dssatoutput=True)  
  # directory = [x.upper() for x in os.listdir(DSSAT_OUTPUT_PATH)]

  for filename in os.listdir(DSSAT_OUTPUT_PATH): 
    if filename.endswith('.OUT'):
      contents = readFile(DSSAT_OUTPUT_PATH + filename)
      code = filename.split('.')[0]

      tokenList = cleanFile(contents, code)
      output[code] = createHeaderMeaningDict(tokenList, meanings['DATA'])

  if writeFlag: 
    writeToFile(outputFilePath, output, 'w', False, True, True)

genDSSATOutputHeaderMeanings(True)