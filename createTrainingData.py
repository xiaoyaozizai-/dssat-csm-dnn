import os
import sys
import os.path
import numpy as np
import pandas as pd

from helperFxns import (
  readFile,
  loadPickle,
  dropColumns,
  writeToFile,
  readStringCSV,
  mapListToNewDictValues
)

from experimentFileConstants import CROP_FILE_EXT
from parsingConfigs import (
  SOIL_LEVEL,
  EXPERIMENT_READ_PATH, 
  OUTPUT_PATH, 
  CLEANED_FILE_PATH,
  CLEANED_WEATHER_FILENAME,
  CLEANED_SOIL_FILENAME,
  CLEANED_EXPERIMENT_FILENAME,
  CLEANED_PLANTGRO_FILENAME,
  CLEANED_PLANTING_DURTN_FILENAME,
  TDFN,
)

from buildExperimentDictionary import buildExperimentDictionary
from buildWeatherDictionary import buildWeatherDict
from buildPlantingDuration import buildPlantingDuration
from buildGenotypeDict import buildGenotypeDict
from buildSoilDictionary import fastDataBuild
from buildDSSATOutputs import buildPlantGro

pd.options.display.max_rows = 999

class BuildTrainingData(): 
  def __init__(self, experimentList, cropName, createSampleDataFlag=False): 
    '''
      Class for building training data. 
        - Arguments 
          - ExperimentList - List of valid experiments. The data will be build according
            to the configurations of '*TREATMENTS' table. 
          - CropName - name of the crop to be built. 
          - createSampleDataFlag - if true, 
    '''

    self.TABLE_INDICES = {
      'CLTVR': ['C'], 'ENVRNMNT': [], 'FRTLZRS': [], 'FLDS': ['L', 'L'],
      'INTIAL': [], 'IRRIGTN': [], 'RSDUES': [], 'TLLGE': [], 'TRTMNTS': [],
      'PLNTGS': ['P'], 'HRVST': [],
    }

    self.OUTPUTS_TO_BE_EXTRACTED = { 
      'PLANTGRO': ['DAP', 'L#SD', 'LWAD', 'SWAD', 'GWAD', 'RWAD', 'VWAD', 'CWAD', 'PWAD', 
                   'P#AD', 'RDPD', 'RL1D', 'RL2D', 'RL3D', 'RL4D', 'RL5D', 'RL6D', 'PWDD', 
                   'PWTD', 'SLAD', 'CHTD', 'CWID', 'NWAD', 'SNW0C', 'SNW1C'] 
    }

    self.CATEGORICAL_VARS = ['CR', 'INGENO', 'PLME', 'PLDS', 'SMHB', 'SMPX', 'SMKE', 'SCOM']
    self.COLS_TO_BE_DROPPED = {
      'TRTMNTS': ['R', 'O', 'C'], 
      'WTH_DAYINFO': ['DATE'] 
    }

    self.RETAIN_THESE_VARS = {
      'PLNTGS': ['PPOP', 'PPOE', 'PLME', 'PLDS', 'PLRS', 'PLRD', 'PLDP'],
      'WTH_GENINFO': ['LAT', 'LONG', 'ELEV'],
      'WTH_DAYINFO': ['SRAD', 'TMAX', 'TMIN', 'RAIN'],
      'SOIL_LVL': ['SLB', 'SLLL', 'SDUL', 'SSAT', 'SRGF', 'SBDM', 'SLOC',
                   'SLHW', 'SLCL', 'SLSI', 'SLNI' ] }

    self.experimentList = experimentList
    self.cropName = cropName
    self.cropCode = CROP_FILE_EXT[cropName][1:3]
    self.createSampleDataFlag = createSampleDataFlag

    self.soilDict = {}
    self.weatherDict = {}
    self.experimentDict = {}
    self.plantGroOutputs = {}
    self.plantingDuration = {}

    self.cache_soilDailyVal = {}
    self.cache_soilGenInfo = {}
    self.cache_weatherDailyVal = {}
    self.cache_weatherGenInfo = {}

    self.dfAccumulator = {}
    self.trainingDataDict = {}

  def buildInputs(self): 
    print('Executing buildInputs()')
    cleanedSoilPath = CLEANED_FILE_PATH + CLEANED_SOIL_FILENAME
    cleanedWeatherPath = CLEANED_FILE_PATH + CLEANED_WEATHER_FILENAME
    cleanedExpPath = CLEANED_FILE_PATH + cropCode + CLEANED_EXPERIMENT_FILENAME
    cleanedPlantGroPath = CLEANED_FILE_PATH + cropCode + CLEANED_PLANTGRO_FILENAME
    cleanedPlantDurtnPath = CLEANED_FILE_PATH + cropCode + CLEANED_PLANTING_DURTN_FILENAME

    if not os.path.isfile(cleanedSoilPath): 
      print('Building Soil Data Dictionary...')
      fastDataBuild(True)

    if not os.path.isfile(cleanedWeatherPath): 
      print('Building Weather Dictionary...')
      buildWeatherDict(writeFlag=True)

    if not os.path.isfile(cleanedExpPath): 
      print('Parsing Experiment Files...')
      buildExperimentDictionary(self.cropName, True)

    if not os.path.isfile(cleanedPlantDurtnPath): 
      print('Preparing Experiments duration...')
      buildPlantingDuration(self.cropName, True)

    if not os.path.isfile(cleanedPlantGroPath): 
      print('Building DSSAT Outputs...')
      buildPlantGro(self.cropName, True)

    self.soilDict = loadPickle(cleanedSoilPath)
    self.weatherDict = loadPickle(cleanedWeatherPath)
    self.experimentDict = loadPickle(cleanedExpPath)
    self.plantGroOutputs = loadPickle(cleanedPlantGroPath)
    self.plantingDuration = loadPickle(cleanedPlantDurtnPath)

    print('Data Loaded!')

  def build(self):
    for filename in self.experimentList:
      experimentCode = filename.split('.')[0]
      if filename.endswith(CROP_FILE_EXT[self.cropName]):
        self.buildExperiment(experimentCode)

    # The imported variable SOIL_LEVEL pertains to the number of soil data levels soilWeatherDict contains. 
    print('Merging DataFrames...')
    self.trainingDataDict[SOIL_LEVEL] = self.mergeDatasets(self.dfAccumulator[SOIL_LEVEL], axs=0)
    self.oneHotEncode()
    null_columns = self.trainingDataDict[SOIL_LEVEL].columns[self.trainingDataDict[SOIL_LEVEL].isnull().any()]  
      
    if len(null_columns) > 0: 
      print(self.trainingDataDict[SOIL_LEVEL][null_columns].isnull().sum())

    if self.createSampleDataFlag: 
      print('Separating Test Inputs for Trained DNN...')
      self.separateSampleInputs()

    print('Training Data Shape: ' + str(self.trainingDataDict[SOIL_LEVEL].shape))
    print('Compressing Training Data...')
    self.trainingDataDict[SOIL_LEVEL].to_pickle(CLEANED_FILE_PATH + self.cropCode + TDFN)

  def buildExperiment(self, experimentCode): 
    '''
      Creates a dataframe per treatment and appends it to the accumulator. 
      The dataframes are built based on the configurations of the treatments table. 
    '''

    print('Executing buildExperiment()')
    # Create Dataframes out of the experiment buffer strings. 
    dataframesDict = {}
    experiment = self.experimentDict[experimentCode]

    for key, val in experiment.items():
      dataframesDict[key] = {}
      for c in val:  
        if len(self.TABLE_INDICES[key]) > 0:
          dataframesDict[key][c] = self.createCommonDF(experiment[key][c], 
                                                       self.TABLE_INDICES[key][c]) 
        else: 
          dataframesDict[key][c] = self.createCommonDF(experiment[key][c])

    # Build the training data based on the configs of 'TRTMNTS' table. 
    treatmentsDF = dataframesDict['TRTMNTS'][0]
    fieldsDF = dataframesDict['FLDS'][0]
    plantingCondDF = dataframesDict['PLNTGS'][0]
    cultivarDF = dataframesDict['CLTVR'][0]

    # Make sure that the data type of the 'INGENO' column is object.
    cultivarDF['INGENO'] = cultivarDF['INGENO'].astype(object)

    # Retain relevant variables. 
    plantingCondDF = dropColumns(plantingCondDF, 
                                 self.RETAIN_THESE_VARS['PLNTGS'], True)

    # Build training data based on the configuration of the treatments table.
    for row in treatmentsDF.itertuples():
      treatmentLevel = getattr(row, 'N')
      outputs = self.getOutputs(experimentCode, 
                                str(treatmentLevel))

      # Short circuit with incomplete simulation outputs.
      plantingRange = self.evaluateShortCircuits(experimentCode, str(treatmentLevel))
      if not plantingRange: 
        continue
          
      if getattr(row, 'FL') != 0:
        fieldLevel = getattr(row, 'FL')
        plantingCondLevel = getattr(row, 'MP')
        cultivarLevel = getattr(row, 'CU')

        # Get weather station code and soil ID.  
        weatherStation = fieldsDF.at[int(fieldLevel), 'WSTA']
        soilID = fieldsDF.at[int(fieldLevel), 'ID_SOIL']

        plantingStart = int(plantingRange['START'])
        # plantingEnd = int(plantingRange['END'])

        if len(weatherStation) == 4: 
          plantingYear = str(plantingStart)[0:2] + '01'
          weatherStation += plantingYear

        # Cache Dataframes. 
        self.cacheSoilData(soilID)  
        self.cacheWeatherData(weatherStation)

        soilDF = self.cache_soilDailyVal[soilID]
        weatherDF = self.cache_weatherDailyVal[weatherStation]
        soilGenInfoDF = self.cache_soilGenInfo[soilID]
        wthGenInfoDF = self.cache_weatherGenInfo[weatherStation]

        # Select weather rows based on the outputs. 
        days = list(outputs['DAP'].values)
        rows = [plantingStart + x for x in days]
        weatherRows = weatherDF[weatherDF['DATE'].isin(rows)]

        # Retain relevant variables
        weatherRows = dropColumns(weatherRows, self.RETAIN_THESE_VARS['WTH_DAYINFO'], True)      
        wthGenInfoDF = dropColumns(wthGenInfoDF, self.RETAIN_THESE_VARS['WTH_GENINFO'], True)
        soilDF = dropColumns(soilDF, self.RETAIN_THESE_VARS['SOIL_LVL'], True)

        # Flatten the soil data. 
        flattenedSoilDF = soilDF.values.flatten()

        # Build parts of the training data. 
        numSoilLevel = soilDF.shape[0]
        trainingDataSize = outputs.shape[0]
        soilDataRows = pd.DataFrame([flattenedSoilDF] * trainingDataSize)
        cultivarRows = pd.DataFrame([cultivarDF.loc[cultivarLevel]] * trainingDataSize)
        plantingRows = pd.DataFrame([plantingCondDF.loc[plantingCondLevel]] * trainingDataSize)
        soilGenInfoRows = pd.DataFrame([soilGenInfoDF.loc[0]] * trainingDataSize)
        weatherGenInfoRows = pd.DataFrame([wthGenInfoDF.loc[0]] * trainingDataSize)

        dapCol = outputs['DAP']
        
        # Remove 'DAP' from the outputs. 
        outputs = outputs.loc[:, outputs.columns != 'DAP']

        # Copy the outputs without the DAP column and remove the last row of the outputs table. 
        outputRows = outputs[:-1]

        columnNames = [x + '_input' for x in list(outputRows.columns.values)]
        outputRows.columns = columnNames
        zeroRow = pd.DataFrame([[0] * len(list(outputRows.columns.values))], columns=columnNames)

        # Merge dataframes. 
        outputRows = self.mergeDatasets([zeroRow, outputRows], axs=0)

        datasets = [cultivarRows, plantingRows, weatherGenInfoRows, weatherRows, 
                    soilGenInfoRows, soilDataRows, dapCol, outputRows, outputs]

        data = self.mergeDatasets(datasets)

        if not numSoilLevel in self.dfAccumulator: 
          self.dfAccumulator[numSoilLevel] = []

        self.dfAccumulator[numSoilLevel].append(data)
        # print(len(self.dfAccumulator))

  def evaluateShortCircuits(self, experimentCode, treatmentLevel): 
    # Don't build training data for experiments with faulty outputs. 

    plantingRange = None
    try:
      plantingRange = self.plantingDuration[experimentCode][str(treatmentLevel)]
    except KeyError:
      return None

    if plantingRange: 
      # Don't create training data out of experiments with different year of planting start and end.
      if plantingRange['START'][0:2] != plantingRange['END'][0:2]: 
        return None

    return plantingRange

  def cacheWeatherData(self, weatherStation): 
    # Creates a dataframe for weather and caches it in case the subsequent experiments need it. 

    if(not weatherStation in self.cache_weatherDailyVal
       and not weatherStation in self.cache_weatherGenInfo):
      dailyValBufferString = self.weatherDict[weatherStation]['WEATHER_DAILY_VALUES']
      genInfoBufferString = self.weatherDict[weatherStation]['WEATHER_GENERAL_INFO']

      self.cache_weatherDailyVal[weatherStation] = self.createCommonDF(dailyValBufferString)
      self.cache_weatherGenInfo[weatherStation] = self.createCommonDF(genInfoBufferString)

  def cacheSoilData(self, soilID): 
    # Creates a dataframe for soil and caches it in case the subsequent experiments need it. 

    if not soilID in self.cache_soilDailyVal:
      if soilID in self.soilDict: 
        genInfoBufferString = self.soilDict[soilID]['SOIL_GENERAL_INFO']
        soilLeveloneBufferString = self.soilDict[soilID]['SOIL_LEVEL_ONE']
        self.cache_soilDailyVal[soilID] = self.createCommonDF(soilLeveloneBufferString)
        self.cache_soilGenInfo[soilID] = self.createCommonDF(genInfoBufferString)
      else: 
        print('%s not found!' % soilID)

  def separateSampleInputs(self): 
    # Separates dataframes that represent one treatment into the dataframe. 

    counter = 1
    SAMPLE_INPUTS = 10
    dfAccumulator = []
    rangeIndices = []
    indices = list(self.trainingDataDict[SOIL_LEVEL].index[self.trainingDataDict[SOIL_LEVEL]['DAP'] == 0])

    for i in range(0, len(indices) - 1): 
      rangeIndices.append([indices[i], indices[i + 1]])

    # No repetitions.
    randomDataFrames = np.random.choice(len(indices) - 1, SAMPLE_INPUTS, replace=False)
    
    # Get the treatments from the main training data. 
    dfAccumulator = [self.trainingDataDict[SOIL_LEVEL].iloc[indices[x]:indices[x + 1]] for x in randomDataFrames]

    # Drop the chosen treatments from the training data. 
    for x in randomDataFrames: 
      self.trainingDataDict[SOIL_LEVEL].drop(self.trainingDataDict[SOIL_LEVEL].index[indices[x]:indices[x + 1]], inplace=True)

    self.trainingDataDict[SOIL_LEVEL].reset_index(inplace=True, drop=True)

    for df in dfAccumulator: 
      fp = CLEANED_FILE_PATH + 'SB_TEST_INPUT_' + str(counter) + '.gz'
      df.to_pickle(fp)
      counter += 1

  def createCommonDF(self, bufferString, indexString=None): 
    # For segments that only contains one table. 
    return readStringCSV(bufferString, index=indexString) 

  def mergeDatasets(self, dataframesList, resetIndex=True, axs=1): 
    for df in dataframesList:
      # drop the indices
      df.reset_index(inplace=True, drop=resetIndex)
    return pd.concat(dataframesList, axis=axs)

  def oneHotEncode(self): 
    # Convert categorical variable to its binary representation. 
    
    print('Encoding Categorical Variables...')
    for key in self.trainingDataDict: 
      # fill nans of categorical variables. 
      df = self.trainingDataDict[key][self.CATEGORICAL_VARS].fillna('NaN')

      # Generate dummy columns for the variables. 
      dummyDfs = pd.get_dummies(df)

      # Remove the categorical variables and merge the encoded variables to training data.
      self.trainingDataDict[key] = dropColumns(self.trainingDataDict[key], self.CATEGORICAL_VARS)
      self.trainingDataDict[key] = self.mergeDatasets([self.trainingDataDict[key], dummyDfs])

  def getOutputs(self, experimentCode, treatmentCode): 
    bufferString = self.plantGroOutputs[experimentCode][treatmentCode]
    df = self.createCommonDF(bufferString)

    return df[self.OUTPUTS_TO_BE_EXTRACTED['PLANTGRO']]

if len(sys.argv) == 2: 
  code = sys.argv[1].upper()
  if not code in CROP_FILE_EXT: 
    print('Error: Crop not found!')
  else: 
    cropCode = CROP_FILE_EXT[code][1:3]
    readPath = EXPERIMENT_READ_PATH + cropCode + '_Experiments/'
    builder = BuildTrainingData(os.listdir(readPath), code)
    builder.buildInputs()
    builder.build()
else: 
  print('Usage: $ python createTrainingData.py <crop>')
