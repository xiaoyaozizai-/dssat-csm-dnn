import pprint
import pandas as pd
from io import StringIO

from parsingConfigs import (
  GENINFO, 
  LVLONE,  
  LVLTWO,  
  SOILLEVELS,
)

'''
  TODO: 
    - Improve reusability of functions. 
'''

'''
  Counting Functions Results (03/24/2018)
    - Soil Horizon Data (LVLONE)
      - Tables with no missing data (checkTokenCount()): 3494
      - Tables that are POSSIBLY missing some data (checkTokenCount()): 176
      - Number of Unique headers (checkHeaders()): 4
        - {'SLB;SLMH;SLLL;SDUL;SLDA;SLRF;SLKS;SLDM;SLOC;SLCF;SLSI;SLCY;SLNI;SLHW;SLHB;SCEC': 1,
           'SLB;SLMH;SLLL;SDUL;SLSA;SLRF;SLKS;SLDM;SLOC;SLCF;SLSI;SLCY;SLNI;SLHW;SLHB;SCEC': 1,
           'SLB;SLMH;SLLL;SDUL;SSAT;SRGF;SSKS;SBDM;SLOC;SLCL;SLSI;SLCF;SLNI;SLHW;SLHB;SCEC': 49,
           'SLB;SLMH;SLLL;SDUL;SSAT;SRGF;SSKS;SBDM;SLOC;SLCL;SLSI;SLCF;SLNI;SLHW;SLHB;SCEC;SADC': 3619} 
    - Soil General Info: 
      - Tables with no missing data (checkTokenCount()): 3668
      - Tables that are POSSIBLY missing some data (checkTokenCount()): 2
      - Number of Unique headers (checkHeaders()): 3
        - {'SCOM;SALB;SLU1;SLDR;SLRO;SLNF;SLPF;SMHB;SMPX;SMKE': 3668, 
           'SCOM;SALB;SLU1;SLDR;SLRO;SLNF;SLPF;SMHB;SMPX;SMKE;SGRP': 1, 
           'SCOM;SALB;SLUI;SLDR;SLRO;SLNF;SLPF;SMHB;SMPX;SMKE;SRGP': 1}

  NOTES
    - Some tables have a second level which serves as additional information to DSSAT to improve its accuracy or
      compute more results. The researchers (bjmrevilla) decided to not included those (for now) because almost all 
      of them are incomplete. 
'''

pp = pprint.PrettyPrinter(width=200) 

def getPandasDFSizes(bufferStringDict, printFlag=False): 
  outputDict = {}
  for key, val in bufferStringDict.items():
    try:
      [row, col] = pd.read_csv(StringIO(val[LVLONE]), sep=';').shape
      rowColKey = str(row) + ',' + str(col) 
      if not rowColKey in outputDict: 
        outputDict[rowColKey] = 1
      else: 
        outputDict[rowColKey] += 1
    except pd.errors.ParserError: 
      print('%s: Error parsing.' % key)

  if printFlag: 
    pp.pprint(outputDict)

def checkAndCount(dictionary, string): 
  if not string in dictionary: dictionary[string] = 1
  else: dictionary[string] += 1

  return dictionary

def checkHeaders(bufferStringDict, printFlag=False):
  '''
    Returns the header with the most number. 
    Input: 
      - bufferStringDict
      - printFlag - if True, it shows the unique headers and its count. 
  '''
  outputDictLVLONE = {}
  outputDictGENINFO = {}

  for key, val in bufferStringDict.items(): 
    stringTokens = val[LVLONE].split('\n')
    stringGenInfoTokens = val[GENINFO].split('\n')

    headerLVLONE = stringTokens[0]
    headerGENINFO = stringGenInfoTokens[0]
  
    outputDictLVLONE = checkAndCount(outputDictLVLONE, headerLVLONE)
    outputDictGENINFO = checkAndCount(outputDictGENINFO, headerGENINFO)

  if printFlag: 
    pp.pprint(outputDictLVLONE)
    pp.pprint(outputDictGENINFO)

  outputDict = {
    'LVLONE': '',
    'GENINFO': ''
  }

  maxValLVLONE = max(outputDictLVLONE.values())
  maxValGENINFO = max(outputDictGENINFO.values())

  for k, v in outputDictGENINFO.items():
    if v == maxValGENINFO:
      outputDict['GENINFO'] = k    
      break
  
  for k, v in outputDictLVLONE.items():
    if v == maxValLVLONE:
      outputDict['LVLONE'] = k    
      break

  return outputDict

def checkTokenCount(bufferStringDict, printFlag=False):
  '''
    Returns list of keys of soil tables. These soil tables were determined 
    to POSSIBLY have incomplete data. 
    
    Input: 
      - bufferStringDict
      - printFlag - if True, it shows the unique headers and its count. 
  '''

  problematicSoilTables = {}
  outputDictLVLONE = {'EQUAL': 0, 'NOT_EQUAL': 0}
  outputDictGENINFO = {'EQUAL': 0, 'NOT_EQUAL': 0}

  for key, val in bufferStringDict.items():
    equalFlagLVLONE = True

    stringTokens = val[LVLONE].split('\n')
    stringGenInfoTokens = val[GENINFO].split('\n')

    headerLVLONELength = len(stringTokens[0].split(';'))
    headerGENINFOLength = len(stringGenInfoTokens[0].split(';'))

    if len(stringGenInfoTokens) == 2: 
      if headerGENINFOLength != len(stringGenInfoTokens[1].split(';')):
        outputDictGENINFO['NOT_EQUAL'] += 1
        if not key in problematicSoilTables: 
          problematicSoilTables[key] = False
      else: 
        outputDictGENINFO['EQUAL'] += 1
      
    for i in range(1, len(stringTokens)): 
      if len(stringTokens[i].split(';')) != headerLVLONELength:
        outputDictLVLONE['NOT_EQUAL'] += 1
        equalFlagLVLONE = False
        if not key in problematicSoilTables: 
          problematicSoilTables[key] = False
        break

    if equalFlagLVLONE: 
      outputDictLVLONE['EQUAL'] += 1

  if printFlag: 
    pp.pprint(outputDictLVLONE)
    pp.pprint(outputDictGENINFO)

  return problematicSoilTables