# Parses the weather files in weather folder. 

'''
  - Check if the parsed weather files is correct. 
'''

import os
import re
import pprint
import pandas as pd
from io import StringIO

from helperFxns import (
  checkSubstring, 
  readFile, 
  pickleDictionary,
  checkDictionaryValues,
  checkStringStart,
  joinTokens,
  readStringCSV
)

from parsingConfigs import (
  WEATHER_READ_PATH,
  EXCLUDE_WTH,
  INCOMPLETE_WTH,
  MISSING_DATA,
  OUTPUT_PATH, 
  WTH_GENINFO,
  WTH_DAILYVAL,
  CLEANED_WEATHER_FILENAME,
  CLEANED_FILE_PATH
)

# Dictionary Keys
REMOVE_THIS = '[±→@\x1a]'

regexObj = re.compile(REMOVE_THIS)

def getAllCompleteWeatherData(weatherTables): 
  cols_geninfo = ['LAT', 'LONG', 'ELEV']
  cols_dailyVal = ['SRAD', 'TMAX', 'TMIN', 'RAIN']

  incompleteWeather = []
  completeWeather = []

  for key, val in weatherTables.items(): 
    data_geninfo = val[WTH_GENINFO]
    data_dailyval = val[WTH_DAILYVAL]
    ddf_geninfo = pd.read_csv(StringIO(data_geninfo), sep=';')
    ddf_dailyVal = pd.read_csv(StringIO(data_dailyval), sep=';')
    
    df_geninfo = ddf_geninfo[cols_geninfo]
    df_dailyVal = ddf_dailyVal[cols_dailyVal]

    null_columns_gi = df_geninfo.columns[df_geninfo.isnull().any()]
    null_columns_dv = df_dailyVal.columns[df_dailyVal.isnull().any()]

    if len(null_columns_gi) != 0: 
      incompleteWeather.append(key)
    elif len(null_columns_dv) != 0: 
      incompleteWeather.append(key)
    else:
      row, col = df_dailyVal.shape
      if row != 365 and row != 366: 
        print(key, df_dailyVal.shape)

      completeWeather.append(key)

  # print('Incomplete: ')
  # print(incompleteWeather)
  # print('Incomplete: ' + str(len(incompleteWeather)))
  # print('Complete: ' + str(len(completeWeather)))

def testData(processedData): 
  # geninfo_cols = ['ELEV'] - use this when detecting null values. 
  geninfo_cols = ['LAT', 'LONG', 'ELEV']
  dailyval_cols = ['SRAD', 'TMAX', 'TMIN', 'RAIN']

  for code, data in processedData.items(): 
    try:
      df1 = readStringCSV(data[WTH_GENINFO])
      df2 = readStringCSV(data[WTH_DAILYVAL])

      print(df2.dtypes)
      # print(df1.select_dtypes(include=['object']))
      # print(df2.select_dtypes(include=['object']))

      # Checking if there is a weather dataset that does not 
      # have required variables. 

      # params_1 = list(df1.columns.values)
      # params_2 = list(df2.columns.values)

      # for col in geninfo_cols: 
      #   if not col in params_1: 
      #     print('GENINFO: ' + code)

      # for col in dailyval_cols: 
      #   if not col in params_2: 
      #     print('DAILYVAL: ' + code)

      # Get columns with null values. 
      # nc_1 = list(df1.columns[df1.isnull().any()])
      # nc_2 = list(df2.columns[df2.isnull().any()])

      # Print weather datasets with missing required data. 
      # if(not len(nc_1) == 0): 
      #   for col in geninfo_cols:
      #     if col in nc_1: 
      #       print('GENINFO: ' + code)

      # if(not len(nc_2) == 0): 
      #   for col in dailyval_cols: 
      #     if col in nc_2: 
      #       print('DAILYVAL: ' + code, nc_2)
      #       break
        
    except pd.errors.ParserError:
      lg.error(code + ': Pandas parsing error. Skipping')
    except KeyError:
      lg.error(code + ': KeyError. Skipping...')

def testTokens(tokens, code): 
  for token in tokens: 
    if token: 
      try: 
        float(token)
      except ValueError: 
        with open(OUTPUT_PATH + 'weatherDebug.txt', 'a') as fp: 
          fp.write(code + ' : ')
          fp.write(str(tokens))
          fp.write('\n')
          break

def processWeatherFile(contents, weatherCode): 
  outputDict = {}
  currentHeader = ''
  headerParametersLength = 0
  stringAccumulator = []

  GENINFO_STR_LEN = 30
  DAILYVAL_STR_LEN = 29
  DV_POSNS = {
    'DATE': [0, 6],
    'SRAD': [7, 11],
    'TMAX': [12, 17],
    'TMIN': [18, 23],
    'RAIN': [24, 29],
  }

  GI_POSNS = {
    'INSI': [0, 6], 
    'LAT': [7, 15], 
    'LONG': [16, 24], 
    'ELEV': [25, 30], 
  }

  WORD_ARRAY = ['INSI', 'LAT', 'LONG', 'ELEV']
  WORD_ARRAY2 = ['SRAD', 'TMAX', 'TMIN', 'RAIN']
  NULL_SIGNS  = ['-99']
  EXCLUDE_THIS = ['*', '!', '==']
  
  for string in contents: 
    if string and not checkStringStart(string, EXCLUDE_THIS):
      # Header
      if string.startswith('@'): 
        if checkSubstring(string, WORD_ARRAY):
          string = string[0:GENINFO_STR_LEN]
          currentHeader = WTH_GENINFO
        elif checkSubstring(string, WORD_ARRAY2): 
          string = string[0:DAILYVAL_STR_LEN]
          outputDict[WTH_GENINFO] = joinTokens(stringAccumulator, '\n')
          stringAccumulator = []
          currentHeader = WTH_DAILYVAL

        tokens = regexObj.sub('', string).upper().split()
        if tokens[0] == 'YRDAY': 
          tokens[0] = 'DATE'

        headerParametersLength = len(tokens)
        stringAccumulator.append(joinTokens(tokens))
      else:
        if currentHeader: 
          if currentHeader == WTH_GENINFO: 
            string = string[0:GENINFO_STR_LEN]
          elif currentHeader == WTH_DAILYVAL: 
            string = string[0:DAILYVAL_STR_LEN]

          tokens = []
          if currentHeader == WTH_DAILYVAL:             
            tokens.append(string[DV_POSNS['DATE'][0]:DV_POSNS['DATE'][1]])
            tokens.append(string[DV_POSNS['SRAD'][0]:DV_POSNS['SRAD'][1]])
            tokens.append(string[DV_POSNS['TMAX'][0]:DV_POSNS['TMAX'][1]])
            tokens.append(string[DV_POSNS['TMIN'][0]:DV_POSNS['TMIN'][1]])
            tokens.append(string[DV_POSNS['RAIN'][0]:DV_POSNS['RAIN'][1]])
          else: 
            tokens.append(string[GI_POSNS['INSI'][0]:GI_POSNS['INSI'][1]])
            tokens.append(string[GI_POSNS['LAT'][0]:GI_POSNS['LAT'][1]])
            tokens.append(string[GI_POSNS['LONG'][0]:GI_POSNS['LONG'][1]])
            tokens.append(string[GI_POSNS['ELEV'][0]:GI_POSNS['ELEV'][1]])

          cleanedTokens = [regexObj.sub('', token) for token in tokens]
          tokens = [x if x and not checkSubstring(x, NULL_SIGNS, True) else '' for x in cleanedTokens]
          
          # if currentHeader == WTH_DAILYVAL: 
          #   testTokens(tokens, weatherCode)

          # If the string is not empty. 
          if ''.join(tokens[0:headerParametersLength]): 
            stringAccumulator.append(joinTokens(tokens[0:headerParametersLength]))
    
  outputDict[currentHeader] = joinTokens(stringAccumulator, '\n')
  return outputDict

def parseWeatherList(): 
  weatherDict = buildWeatherDict()
  contents = readFile(WEATHER_READ_PATH + 'WTH.LST')
  weatherFileList = []
  NOT_DATA = ['*', '@']

  for string in contents: 
    if(string and not checkStringStart(string, NOT_DATA)): 
      weatherCode = string.split()[1].upper()
      if(not checkStringStart(weatherCode, EXCLUDE_WTH)
         and not weatherCode in INCOMPLETE_WTH
         and not weatherCode in MISSING_DATA): 
        weatherFileList.append(string.split()[1])

  return weatherFileList

def buildWeatherDict(buildAll=True, filename='', writeFlag=False, test=False): 
  if buildAll:
    weatherDict = {} 
    for filename in os.listdir(WEATHER_READ_PATH):
      if(filename.endswith('.WTH') 
         or filename.endswith('.wth')):
        code = filename.split('.')[0].upper()
        if(not checkStringStart(code, EXCLUDE_WTH)
           and not code in INCOMPLETE_WTH
           and not code in MISSING_DATA):
          
          contents = readFile(WEATHER_READ_PATH + filename)
          weatherDict[code] = processWeatherFile(contents, code)

    if test: 
      # testData(weatherDict)
      getAllCompleteWeatherData(weatherDict)
      # parseWeatherList()

    if writeFlag: 
      writePath = CLEANED_FILE_PATH + CLEANED_WEATHER_FILENAME
      pickleDictionary(writePath, weatherDict, True)

    return weatherDict
  else: 
    filename = filename + '.WTH'
    if(filename in os.listdir(WEATHER_READ_PATH)
       and not checkStringStart(filename, EXCLUDE_WTH)
       and not filename in INCOMPLETE_WTH): 
      contents = readFile(WEATHER_READ_PATH + filename) 
      return processWeatherFile(contents, code)

# buildWeatherDict()
# buildWeatherDict(writeFlag=True)
# buildWeatherDict(test=True)
# buildWeatherDict(True, writeFlag=True)
